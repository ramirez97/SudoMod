# **SUDOMOD**

***

## Getting Started

If you're comfortable getting up and running from a `git clone`, this method is for you.

If you clone the GitHub repository, you are ready to go!

The [master](https://bitbucket.org/Ramirez97/iplug-raraudio/src/03855cb31b7e67fa459ef1e7975c42730fcabe61?at=master) branch which contains the latest release.

#### Quickstart:

1. Update CrossMap and Natives for latest Online Version

## Versioning

For transparency and insight into our release cycle, and for striving to maintain backward compatibility, _**SUDOMOD**_ will be maintained according to the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>-<build>`

Constructed with the following guidelines:

* A new *major* release indicates a large change where backward compatibility is broken.
* A new *minor* release indicates a normal change that maintains backward compatibility.
* A new *patch* release indicates a bugfix or small change which does not affect compatibility.
* A new *build* release indicates this is a pre-release of the version.

***

##### PS: This repo may or may not be updated

***