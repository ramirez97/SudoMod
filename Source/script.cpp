#include "stdafx.h"

/*
	THIS FILE IS A PART OF GTA V SCRIPT HOOK SDK
				http://dev-c.com			
			(C) Alexander Blade 2015
*/

/*
	F4					activate
	NUM2/8/4/6			navigate thru the menus and lists (numlock must be on)
	NUM5 				select
	NUM0/BACKSPACE/F4 	back
	W/S Key 			use vehicle boost when active
	~ KEY				use vehicle rockets when active
	W + E key			throw rpg knife
	T KEY				place blimp to teleport with t key on your keyboard
*/

#include "stdafx.h"

std::string statusText;
DWORD statusTextDrawTicksMax;
bool statusTextGxtEntry;

void update_status_text()
{
	if (GetTickCount() < statusTextDrawTicksMax)
	{
		UI::SET_TEXT_FONT(7);
		UI::SET_TEXT_SCALE(0.6f, 0.6f);
		UI::SET_TEXT_COLOUR(255, 255, 255, 255);
		UI::SET_TEXT_WRAP(0.0f, 1.0f);
		UI::SET_TEXT_CENTRE(1);
		UI::SET_TEXT_OUTLINE();
		UI::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
		UI::SET_TEXT_EDGE(1, 0, 0, 0, 205);
		if (statusTextGxtEntry)
		{
			UI::_SET_TEXT_ENTRY((char *)statusText.c_str());
		} else
		{
			UI::_SET_TEXT_ENTRY("STRING");
			UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME((char *)statusText.c_str());
		}
		UI::_DRAW_TEXT(0.5, 0);
		//UI::_DRAW_TEXT(0.6f, 0.6f);
	}
}

void set_status_text(std::string str, DWORD time, bool isGxtEntry)
{
	statusText = str;
	statusTextDrawTicksMax = GetTickCount() + time;
	statusTextGxtEntry = isGxtEntry;
}

// features
bool featureTeleportKey = false;
bool featurePlayerInvincible = false;
bool featurePlayerInvincibleUpdated = false;
bool featurePlayerInvisible = false;
bool featurePlayerNeverWanted = false;
bool featurePlayerIgnored = false;
bool featurePlayerIgnoredUpdated = false;
bool featurePlayerUnlimitedAbility = false;
bool featurePlayerNoNoise = false;
bool featurePlayerNoNoiseUpdated = false;
bool featurePlayerFastSwim = false;
bool featurePlayerFastSwimUpdated = false;
bool featurePlayerFastRun = false;
bool featurePlayerFastRunUpdated = false;
bool featurePlayerSuperJump = false;
//weapons
bool featureWeaponOneHit = false;
bool featureWeaponNoReload = false;
bool featureWeaponFireAmmo = false;
bool featureWeaponExplosiveAmmo = false;
bool featureWeaponExplosiveMelee = false;
bool featureWeaponTeleportToImpact = false;
bool featureWeaponVehRockets = false;
bool featureWeaponPlaRockets = false;
bool featureObjectGun = false;
bool featureObjectGun1 = false;
bool featureObjectGun2 = false;
DWORD featureWeaponVehShootLastTime = 0;
//vehicles
bool featureVehInvincible = false;
bool featureVehInvincibleUpdated = false;
bool featureVehInvincibleWheels = false;
bool featureVehInvincibleWheelsUpdated = false;
bool featureVehSeatbelt = false;
bool featureVehSeatbeltUpdated = false;
bool featureVehSpeedBoost = false;
bool featureVehWrapInSpawned = false;
bool featureVehRainbow = false;
bool featureVehStickyCar = false;
bool featureVehSlide = false;
//world
bool featureWorldBlackOut = false;
bool featureWorldMoonGravity = false;
bool featureWorldRandomCops = true;
bool featureWorldRandomTrains = true;
bool featureWorldRandomBoats = true;
bool featureWorldGarbageTrucks = true;
bool featureHudFx = false;
bool featureHudFx1 = false;
bool featureHudFx2 = false;
bool featureHudFx3 = false;
bool featureHudFx4 = false;
bool featureHudFx5 = false;
bool featureHudFx6 = false;
bool featureHudFx7 = false;
bool featureHudFx8 = false;
bool featureHudFx9 = false;
bool featureHudFx10 = false;
bool featureHudFx11 = false;
bool featureHudFx12 = false;
bool featureHudFx13 = false;
bool featureHudFx14 = false;
bool featureHudFx15 = false;
//time
bool featureTimePaused = false;
bool featureTimePausedUpdated = false;
bool featureTimeSynced = false;
//weather
bool featureWeatherWind = false;
bool featureWeatherPers = false;
//mic
bool featureMiscMobileRadio = true;
bool featureMiscLockRadio = false;
bool featureMiscHideHud = false;
// player model control, switching on normal ped model when needed	
void check_player_model() 
{
	// common variables
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();	

	if (!ENTITY::DOES_ENTITY_EXIST(playerPed)) return;

	Hash model = ENTITY::GET_ENTITY_MODEL(playerPed);
	if (ENTITY::IS_ENTITY_DEAD(playerPed) || PLAYER::IS_PLAYER_BEING_ARRESTED(player, TRUE))
		if (model != $("player_zero") && 
			model != $("player_one") &&
			model != $("player_two"))
		{
			set_status_text("turning to normal");
			WAIT(1000);

			model = $("player_zero");
			STREAMING::REQUEST_MENU_PED_MODEL(model);
			while (!STREAMING::HAS_MODEL_LOADED(model))
				WAIT(0);
			PLAYER::SET_PLAYER_MODEL(PLAYER::PLAYER_ID(), model);
			PED::SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER::PLAYER_PED_ID());
			STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);

			// wait until player is ressurected
			while (ENTITY::IS_ENTITY_DEAD(PLAYER::PLAYER_PED_ID()) || PLAYER::IS_PLAYER_BEING_ARRESTED(player, TRUE))
				WAIT(0);

		}
}

void update_player_rockets() {
	Vector3 Rot = CAM::GET_GAMEPLAY_CAM_ROT(0);
	Vector3 Dir = rot_to_direction(&Rot);
	Vector3 CPosition = CAM::GET_GAMEPLAY_CAM_COORD();
	Vector3 PlayerPosition = ENTITY::GET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), 1);
	float spawnDistance = get_distance(&CPosition, &PlayerPosition);
	spawnDistance += 2;
	Vector3 SPosition = add(&CPosition, &multiply(&Dir, spawnDistance));
	DWORD Model = $("w_me_knife_01");

	if (featureWeaponPlaRockets) {
			Object ObJect = OBJECT::CREATE_OBJECT(Model, SPosition.x, SPosition.y, SPosition.z, 1, 1, 1);
			STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(Model);

			if (ENTITY::DOES_ENTITY_EXIST(ObJect)) {
				ENTITY::SET_ENTITY_RECORDS_COLLISIONS(ObJect, true);
				for (float f = 0.0f; f<75.0f; f++) {
					if (ENTITY::HAS_ENTITY_COLLIDED_WITH_ANYTHING(ObJect))break;
					ENTITY::APPLY_FORCE_TO_ENTITY(ObJect, 1, Dir.x*10.0f, Dir.y*10.0f, Dir.z*10.0f, 0.0f, 0.0f, 0.0f, 0, 0, 1, 1, 0, 1);
					WAIT(0);
				}

				Vector3 Coordinates = ENTITY::GET_ENTITY_COORDS(ObJect, 1);
				OBJECT::DELETE_OBJECT(&ObJect);
				FIRE::ADD_EXPLOSION(Coordinates.x, Coordinates.y, Coordinates.z, 9, 25.0f, 1, 1, 0.5f);
		}
	}
}

void update_vehicle_guns() {
	Player Player = PLAYER::PLAYER_ID();
	Ped PlayerPed = PLAYER::PLAYER_PED_ID();

	if (!ENTITY::DOES_ENTITY_EXIST(PlayerPed) || !featureWeaponVehRockets)return;

	bool bSelect = KeyDown(VK_ADD); 
	if (bSelect && featureWeaponVehShootLastTime + 150<GetTickCount() &&
		PLAYER::IS_PLAYER_CONTROL_ON(Player) && PED::IS_PED_IN_ANY_VEHICLE(PlayerPed, 0)) 
	{
		Vehicle VehicleHandle = PED::GET_VEHICLE_PED_IS_USING(PlayerPed);
		Vector3 Coordinates1, Coordinates2;
		GAMEPLAY::GET_MODEL_DIMENSIONS(ENTITY::GET_ENTITY_MODEL(VehicleHandle), &Coordinates1, &Coordinates2);

		Hash WeaponAssetRocket = $("WEAPON_VEHICLE_ROCKET");
		if (!WEAPON::HAS_WEAPON_ASSET_LOADED(WeaponAssetRocket)) 
		{
			WEAPON::REQUEST_WEAPON_ASSET(WeaponAssetRocket, 31, 0);
			while (!WEAPON::HAS_WEAPON_ASSET_LOADED(WeaponAssetRocket))
				WAIT(0);
		}

		Vector3 Coordinates0From = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleHandle, -(Coordinates1.x + 0.25f), Coordinates1.y + 1.25f, 0.1f);
		Vector3 Coordinates1From = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleHandle, (Coordinates1.x + 0.25f), Coordinates1.y + 1.25f, 0.1f);
		Vector3 Coordinates0To = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleHandle, -Coordinates1.x, Coordinates1.y + 100.0f, 0.1f);
		Vector3 Coordinates1To = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleHandle, Coordinates1.x, Coordinates1.y + 100.0f, 0.1f);

		GAMEPLAY::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(Coordinates0From.x, Coordinates0From.y, Coordinates0From.z,
			Coordinates0To.x, Coordinates0To.y, Coordinates0To.z, 250, 1, WeaponAssetRocket, PlayerPed, 1, 0, -1.0);
		GAMEPLAY::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(Coordinates1From.x, Coordinates1From.y, Coordinates1From.z,
			Coordinates1To.x, Coordinates1To.y, Coordinates1To.z, 250, 1, WeaponAssetRocket, PlayerPed, 1, 0, -1.0);
		featureWeaponVehShootLastTime = GetTickCount();
	}
}

//Memory edits
void setRocketState(int s) { Memory::set_value<int>({ OFFSET_PLAYER, OFFSET_PLAYER_VEHICLE, 0x318 }, s); }
int getRocketState() { return Memory::get_value<int>({ OFFSET_PLAYER, OFFSET_PLAYER_VEHICLE, 0x318 }); }

void setRocketCharge(float s) { Memory::set_value<float>({ OFFSET_PLAYER, OFFSET_PLAYER_VEHICLE, 0x31C }, s); }
float getRocketCharge() { return Memory::get_value<float>({ OFFSET_PLAYER, OFFSET_PLAYER_VEHICLE, 0x31C }); }

bool skinchanger_used = false;
bool pedspawn_used = false;
// Updates all features that can be turned off by the game, being called each game frame
void update_features() 
{
	update_status_text();

	update_vehicle_guns();

	if (KeyJustUp(0x45))
		update_player_rockets();
	
	// changing player model if died/arrested while being in another skin, since it can cause inf loading loop
	if (skinchanger_used)
		check_player_model();

	// wait until player is ready, basicly to prevent using the trainer while player is dead or arrested
	while (ENTITY::IS_ENTITY_DEAD(PLAYER::PLAYER_PED_ID()) || PLAYER::IS_PLAYER_BEING_ARRESTED(PLAYER::PLAYER_ID(), TRUE))
		WAIT(0);

	// read default feature values from the game
	featureWorldRandomCops = PED::CAN_CREATE_RANDOM_COPS() == TRUE;

	// common variables
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();	
	BOOL playerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
	Vehicle playerVehicle = PED::GET_VEHICLE_PED_IS_USING(playerPed);
	Vector3 playerCoords = ENTITY::GET_ENTITY_COORDS(playerPed, 1);
	DWORD Model = ENTITY::GET_ENTITY_MODEL(playerVehicle);
	
	/* PLAYER */
		
	// player invincible
	if (featurePlayerInvincibleUpdated)
	{
		if (playerExists && !featurePlayerInvincible)
			PLAYER::SET_PLAYER_INVINCIBLE(player, FALSE);
		featurePlayerInvincibleUpdated = false;
	}
	if (featurePlayerInvincible)
	{
		if (playerExists)
			PLAYER::SET_PLAYER_INVINCIBLE(player, TRUE);
	}
	
	// player invisible
	if (featurePlayerInvisible&&playerExists)
	{
		ENTITY::SET_ENTITY_VISIBLE(playerPed, false, 0);
	}
	else 
	{
		ENTITY::SET_ENTITY_VISIBLE(playerPed, true, 0);
	}

	// player never wanted
	if (featurePlayerNeverWanted)
	{
		if (playerExists)
			PLAYER::CLEAR_PLAYER_WANTED_LEVEL(player);
	}

	// police ignore player
	if (featurePlayerIgnoredUpdated)
	{
		if (playerExists)
			PLAYER::SET_POLICE_IGNORE_PLAYER(player, featurePlayerIgnored);
		featurePlayerIgnoredUpdated = false;
	}

	// player special ability
	if (featurePlayerUnlimitedAbility)
	{
		if (playerExists)
			PLAYER::SPECIAL_ABILITY_FILL_METER(player, 1);
	}

	// player no noise
	if (featurePlayerNoNoiseUpdated)
	{
		if (playerExists && !featurePlayerNoNoise)
			PLAYER::SET_PLAYER_NOISE_MULTIPLIER(player, 1.0f);
		featurePlayerNoNoiseUpdated = false;
	}
	if (featurePlayerNoNoise)
		PLAYER::SET_PLAYER_NOISE_MULTIPLIER(player, 0.0);

	// player fast swim
	if (featurePlayerFastSwimUpdated)
	{
		if (playerExists && !featurePlayerFastSwim)
			PLAYER::SET_SWIM_MULTIPLIER_FOR_PLAYER(player, 1.0f);
		featurePlayerFastSwimUpdated = false;
	}
	if (featurePlayerFastSwim)
		PLAYER::SET_SWIM_MULTIPLIER_FOR_PLAYER(player, 1.49f);

	// player fast run
	if (featurePlayerFastRunUpdated)
	{
		if (playerExists && !featurePlayerFastRun)
			PLAYER::SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(player, 1.0f);
		featurePlayerFastRunUpdated = false;
	}
	if (featurePlayerFastRun)
		PLAYER::SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(player, 1.49f);

	// player super jump
	if (featurePlayerSuperJump)
	{
		if (playerExists)
			GAMEPLAY::SET_SUPER_JUMP_THIS_FRAME(player);
	}

	/* WEAPONS */

	// one hit kill
	if (featureWeaponOneHit)
	{
		if (playerExists)
			PLAYER::SET_PLAYER_WEAPON_DAMAGE_MODIFIER(player, 9999.f);
	}
		
	//fire ammo
	if (featureWeaponFireAmmo)
	{
		if (playerExists)
			GAMEPLAY::SET_FIRE_AMMO_THIS_FRAME(player);
	}
	// explosive ammo
	if (featureWeaponExplosiveAmmo)
	{
		if (playerExists)
			GAMEPLAY::SET_EXPLOSIVE_AMMO_THIS_FRAME(player);
	}
	// explosive melee
	if (featureWeaponExplosiveMelee)
	{
		if (playerExists)
			GAMEPLAY::SET_EXPLOSIVE_MELEE_THIS_FRAME(player);
	}
	// teleport to impact
	if (featureWeaponTeleportToImpact)
	{
		if (playerExists && PED::IS_PED_SHOOTING(playerPed))
		{
			Vector3 iCoord;
			if (WEAPON::GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, &iCoord))
			{
				ENTITY::SET_ENTITY_COORDS(playerPed, iCoord.x, iCoord.y, iCoord.z+1, 0, 0, 1, 1);
				WAIT(0); 
				set_status_text("X:" + std::to_string(iCoord.x) + "\tY:" + std::to_string(iCoord.y) + "\tZ:" + std::to_string(iCoord.z));
			}
		}
	}

	// weapon no reload
	if (playerExists && featureWeaponNoReload)
	{
		static Hash currentWeapon;
		if (WEAPON::GET_CURRENT_PED_WEAPON(playerPed, &currentWeapon, 1))
		{
			if (WEAPON::IS_WEAPON_VALID(currentWeapon))
			{
				int maxAmmo;
				if (WEAPON::GET_MAX_AMMO(playerPed, currentWeapon, &maxAmmo))
				{
					WEAPON::SET_PED_AMMO(playerPed, currentWeapon, maxAmmo);

					maxAmmo = WEAPON::GET_MAX_AMMO_IN_CLIP(playerPed, currentWeapon, 1);
					if (maxAmmo > 0)
						WEAPON::SET_AMMO_IN_CLIP(playerPed, currentWeapon, maxAmmo);
				}
			}
		}
	}
	// money gun
	if (featureObjectGun) {
		if (ENTITY::DOES_ENTITY_EXIST(playerPed)) {
			if (PED::IS_PED_SHOOTING(playerPed)) {
				float Tmp[6];
				WEAPON::GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, (Vector3*)Tmp);
				if (Tmp[0] != 0 || Tmp[2] != 0 || Tmp[4] != 0) {
				OBJECT::CREATE_AMBIENT_PICKUP($("PICKUP_MONEY_MED_BAG"), Tmp[0], Tmp[2], Tmp[4], 0, 40000, $("prop_gold_bar"), FALSE, TRUE);
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED($("prop_gold_bar")); }
			}
		}
	}
	if (featureObjectGun1) {
		if (ENTITY::DOES_ENTITY_EXIST(playerPed)) {
			if (PED::IS_PED_SHOOTING(playerPed)) {
				float Tmp[6];
				WEAPON::GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, (Vector3*)Tmp);
				if (Tmp[0] != 0 || Tmp[2] != 0 || Tmp[4] != 0) {	
				OBJECT::CREATE_AMBIENT_PICKUP($("PICKUP_ARMOUR_STANDARD"), Tmp[0], Tmp[2], Tmp[4], 0, 999999999, $("prop_armour_pickup"), FALSE, TRUE);
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED($("prop_armour_pickup")); }
			}
		}
	}
	if (featureObjectGun2) {
		if (ENTITY::DOES_ENTITY_EXIST(playerPed)) {
			if (PED::IS_PED_SHOOTING(playerPed)) {
				float Tmp[6];
				WEAPON::GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, (Vector3*)Tmp);
				if (Tmp[0] != 0 || Tmp[2] != 0 || Tmp[4] != 0) {
				OBJECT::CREATE_AMBIENT_PICKUP($("PICKUP_HEALTH_STANDARD"), Tmp[0], Tmp[2], Tmp[4], 0, 999999999, $("prop_energy_drink"), FALSE, TRUE);
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED($("prop_energy_drink")); }
			}
		}
	}
	// player's vehicle invincible
	if (playerVehicle && featureVehInvincibleUpdated) {
		if (playerExists && !featureVehInvincible&&PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0)) {
			ENTITY::SET_ENTITY_INVINCIBLE(playerVehicle, FALSE);
			ENTITY::SET_ENTITY_PROOFS(playerVehicle, 0, 0, 0, 0, 0, 0, 0, 0);
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(playerVehicle, 1);
			VEHICLE::SET_VEHICLE_WHEELS_CAN_BREAK(playerVehicle, 1);
			VEHICLE::SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(playerVehicle, 1);
		}
		featureVehInvincibleUpdated = false;
	}
	if (playerVehicle && featureVehInvincible) {
		if (playerExists&&PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0)) {
			ENTITY::SET_ENTITY_INVINCIBLE(playerVehicle, TRUE);
			ENTITY::SET_ENTITY_PROOFS(playerVehicle, 1, 1, 1, 1, 1, 1, 1, 1);
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(playerVehicle, 0);
			VEHICLE::SET_VEHICLE_WHEELS_CAN_BREAK(playerVehicle, 0);
			VEHICLE::SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(playerVehicle, 0);
		}
	}
	// player's vehicle invincible wheels, usefull with custom handling
	if (playerVehicle && featureVehInvincibleWheelsUpdated) {
		if (playerExists && !featureVehInvincibleWheels&&PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0)) {
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(playerVehicle, TRUE);
			VEHICLE::SET_VEHICLE_WHEELS_CAN_BREAK(playerVehicle, TRUE);
			VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(playerVehicle, FALSE);
		}
		featureVehInvincibleWheelsUpdated = false;
	}
	if (playerVehicle && featureVehInvincibleWheels) {
		if (playerExists&&PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0)) {
			VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(playerVehicle, FALSE);
			VEHICLE::SET_VEHICLE_WHEELS_CAN_BREAK(playerVehicle, FALSE);
			VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(playerVehicle, TRUE);
		}
	}
	// seat belt
	const int PED_FLAG_CAN_FLY_THRU_WINDSCREEN = 32;
	if (featureVehSeatbeltUpdated) {
		if (playerExists && !featureVehSeatbelt)
			PED::SET_PED_CONFIG_FLAG(playerPed, PED_FLAG_CAN_FLY_THRU_WINDSCREEN, TRUE);
		featureVehSeatbeltUpdated = false;
	}
	if (featureVehSeatbelt) {
		if (playerExists) {
			if (PED::GET_PED_CONFIG_FLAG(playerPed, PED_FLAG_CAN_FLY_THRU_WINDSCREEN, TRUE))
				PED::SET_PED_CONFIG_FLAG(playerPed, PED_FLAG_CAN_FLY_THRU_WINDSCREEN, FALSE);
		}
	}
	// player's vehicle boost
	if (playerVehicle && featureVehSpeedBoost&&playerExists) {

		bool boostKey = KeyDown(VK_NUMPAD9);
		bool stopKey = KeyDown(VK_NUMPAD3);
		if (VEHICLE::_HAS_VEHICLE_ROCKET_BOOST(playerVehicle))
		{
			if (boostKey)
			{
				if (getRocketCharge() < 2.5f) setRocketCharge(2.5f);
				if (!VEHICLE::_IS_VEHICLE_ROCKET_BOOST_ACTIVE(playerVehicle)) VEHICLE::_SET_VEHICLE_ROCKET_BOOST_ACTIVE(playerVehicle, true);
			}
			else VEHICLE::_SET_VEHICLE_ROCKET_BOOST_ACTIVE(playerVehicle, false);
		}
		else
		{
			float Speed = ENTITY::GET_ENTITY_SPEED(playerVehicle);
			if (Speed<3.0f)Speed = 3.0f;
			Speed += Speed*0.05f;
			VEHICLE::SET_VEHICLE_FORWARD_SPEED(playerVehicle, Speed);
		}
		if (stopKey)
		{
				VEHICLE::SET_VEHICLE_FORWARD_SPEED(playerVehicle, 0.0);
		}
	}

	// slidy car
	if (playerVehicle && featureVehSlide) {
		VEHICLE::SET_VEHICLE_REDUCE_GRIP(PED::GET_VEHICLE_PED_IS_IN(PLAYER::PLAYER_PED_ID(), 0), 1);
	}
	else {
		VEHICLE::SET_VEHICLE_REDUCE_GRIP(PED::GET_VEHICLE_PED_IS_IN(PLAYER::PLAYER_PED_ID(), 0), 0);
	}
	// rainbow car
	if (playerVehicle && featureVehRainbow) {
		VEHICLE::SET_VEHICLE_COLOURS(playerVehicle, rand() % 157, rand() % 157);
	}
	// sticky car
	if (playerVehicle && featureVehStickyCar) {
		if (PED::IS_PED_IN_ANY_VEHICLE(PLAYER::PLAYER_PED_ID(), 0)) {
			int PVehicleHandle = PED::GET_VEHICLE_PED_IS_USING(PLAYER::PLAYER_PED_ID());
			VEHICLE::SET_VEHICLE_ON_GROUND_PROPERLY(PVehicleHandle);
		}
	}

	// time pause
	if (featureTimePausedUpdated)
	{
		TIME::PAUSE_CLOCK(featureTimePaused);
		featureTimePausedUpdated = false;
	}

	// time sync
	if (featureTimeSynced)
	{
		static time_t CTime = time(0);
		static tm T;
		localtime_s(&T, &CTime);
		TIME::SET_CLOCK_TIME(T.tm_hour, T.tm_min, T.tm_sec);
	}

	// hide hud
	if (featureMiscHideHud)
		UI::HIDE_HUD_AND_RADAR_THIS_FRAME();
}

LPCSTR pedModels[70][10] = {
	{"player_zero", "player_one", "player_two", "a_c_boar", "a_c_chimp", "a_c_cow", "a_c_coyote", "a_c_deer", "a_c_fish", "a_c_hen"},
	{ "a_c_cat_01", "a_c_chickenhawk", "a_c_cormorant", "a_c_crow", "a_c_dolphin", "a_c_humpback", "a_c_killerwhale", "a_c_pigeon", "a_c_seagull", "a_c_sharkhammer"},
	{"a_c_pig", "a_c_rat", "a_c_rhesus", "a_c_chop", "a_c_husky", "a_c_mtlion", "a_c_retriever", "a_c_sharktiger", "a_c_shepherd", "s_m_m_movalien_01"},
	{"a_f_m_beach_01", "a_f_m_bevhills_01", "a_f_m_bevhills_02", "a_f_m_bodybuild_01", "a_f_m_business_02", "a_f_m_downtown_01", "a_f_m_eastsa_01", "a_f_m_eastsa_02", "a_f_m_fatbla_01", "a_f_m_fatcult_01"},
	{"a_f_m_fatwhite_01", "a_f_m_ktown_01", "a_f_m_ktown_02", "a_f_m_prolhost_01", "a_f_m_salton_01", "a_f_m_skidrow_01", "a_f_m_soucentmc_01", "a_f_m_soucent_01", "a_f_m_soucent_02", "a_f_m_tourist_01"},
	{"a_f_m_trampbeac_01", "a_f_m_tramp_01", "a_f_o_genstreet_01", "a_f_o_indian_01", "a_f_o_ktown_01", "a_f_o_salton_01", "a_f_o_soucent_01", "a_f_o_soucent_02", "a_f_y_beach_01", "a_f_y_bevhills_01"},
	{"a_f_y_bevhills_02", "a_f_y_bevhills_03", "a_f_y_bevhills_04", "a_f_y_business_01", "a_f_y_business_02", "a_f_y_business_03", "a_f_y_business_04", "a_f_y_eastsa_01", "a_f_y_eastsa_02", "a_f_y_eastsa_03"},
	{"a_f_y_epsilon_01", "a_f_y_fitness_01", "a_f_y_fitness_02", "a_f_y_genhot_01", "a_f_y_golfer_01", "a_f_y_hiker_01", "a_f_y_hippie_01", "a_f_y_hipster_01", "a_f_y_hipster_02", "a_f_y_hipster_03"},
	{"a_f_y_hipster_04", "a_f_y_indian_01", "a_f_y_juggalo_01", "a_f_y_runner_01", "a_f_y_rurmeth_01", "a_f_y_scdressy_01", "a_f_y_skater_01", "a_f_y_soucent_01", "a_f_y_soucent_02", "a_f_y_soucent_03"},
	{"a_f_y_tennis_01", "a_f_y_topless_01", "a_f_y_tourist_01", "a_f_y_tourist_02", "a_f_y_vinewood_01", "a_f_y_vinewood_02", "a_f_y_vinewood_03", "a_f_y_vinewood_04", "a_f_y_yoga_01", "a_m_m_acult_01"},
	{"a_m_m_afriamer_01", "a_m_m_beach_01", "a_m_m_beach_02", "a_m_m_bevhills_01", "a_m_m_bevhills_02", "a_m_m_business_01", "a_m_m_eastsa_01", "a_m_m_eastsa_02", "a_m_m_farmer_01", "a_m_m_fatlatin_01"},
	{"a_m_m_genfat_01", "a_m_m_genfat_02", "a_m_m_golfer_01", "a_m_m_hasjew_01", "a_m_m_hillbilly_01", "a_m_m_hillbilly_02", "a_m_m_indian_01", "a_m_m_ktown_01", "a_m_m_malibu_01", "a_m_m_mexcntry_01"},
	{"a_m_m_mexlabor_01", "a_m_m_og_boss_01", "a_m_m_paparazzi_01", "a_m_m_polynesian_01", "a_m_m_prolhost_01", "a_m_m_rurmeth_01", "a_m_m_salton_01", "a_m_m_salton_02", "a_m_m_salton_03", "a_m_m_salton_04"},
	{"a_m_m_skater_01", "a_m_m_skidrow_01", "a_m_m_socenlat_01", "a_m_m_soucent_01", "a_m_m_soucent_02", "a_m_m_soucent_03", "a_m_m_soucent_04", "a_m_m_stlat_02", "a_m_m_tennis_01", "a_m_m_tourist_01"},
	{"a_m_m_trampbeac_01", "a_m_m_tramp_01", "a_m_m_tranvest_01", "a_m_m_tranvest_02", "a_m_o_acult_01", "a_m_o_acult_02", "a_m_o_beach_01", "a_m_o_genstreet_01", "a_m_o_ktown_01", "a_m_o_salton_01"},
	{"a_m_o_soucent_01", "a_m_o_soucent_02", "a_m_o_soucent_03", "a_m_o_tramp_01", "a_m_y_acult_01", "a_m_y_acult_02", "a_m_y_beachvesp_01", "a_m_y_beachvesp_02", "a_m_y_beach_01", "a_m_y_beach_02"},
	{"a_m_y_beach_03", "a_m_y_bevhills_01", "a_m_y_bevhills_02", "a_m_y_breakdance_01", "a_m_y_busicas_01", "a_m_y_business_01", "a_m_y_business_02", "a_m_y_business_03", "a_m_y_cyclist_01", "a_m_y_dhill_01"},
	{"a_m_y_downtown_01", "a_m_y_eastsa_01", "a_m_y_eastsa_02", "a_m_y_epsilon_01", "a_m_y_epsilon_02", "a_m_y_gay_01", "a_m_y_gay_02", "a_m_y_genstreet_01", "a_m_y_genstreet_02", "a_m_y_golfer_01"},
	{"a_m_y_hasjew_01", "a_m_y_hiker_01", "a_m_y_hippy_01", "a_m_y_hipster_01", "a_m_y_hipster_02", "a_m_y_hipster_03", "a_m_y_indian_01", "a_m_y_jetski_01", "a_m_y_juggalo_01", "a_m_y_ktown_01"},
	{"a_m_y_ktown_02", "a_m_y_latino_01", "a_m_y_methhead_01", "a_m_y_mexthug_01", "a_m_y_motox_01", "a_m_y_motox_02", "a_m_y_musclbeac_01", "a_m_y_musclbeac_02", "a_m_y_polynesian_01", "a_m_y_roadcyc_01"},
	{"a_m_y_runner_01", "a_m_y_runner_02", "a_m_y_salton_01", "a_m_y_skater_01", "a_m_y_skater_02", "a_m_y_soucent_01", "a_m_y_soucent_02", "a_m_y_soucent_03", "a_m_y_soucent_04", "a_m_y_stbla_01"},
	{"a_m_y_stbla_02", "a_m_y_stlat_01", "a_m_y_stwhi_01", "a_m_y_stwhi_02", "a_m_y_sunbathe_01", "a_m_y_surfer_01", "a_m_y_vindouche_01", "a_m_y_vinewood_01", "a_m_y_vinewood_02", "a_m_y_vinewood_03"},
	{"a_m_y_vinewood_04", "a_m_y_yoga_01", "u_m_y_proldriver_01", "u_m_y_rsranger_01", "u_m_y_sbike", "u_m_y_staggrm_01", "u_m_y_tattoo_01", "csb_abigail", "csb_anita", "csb_anton"},
	{"csb_ballasog", "csb_bride", "csb_burgerdrug", "csb_car3guy1", "csb_car3guy2", "csb_chef", "csb_chin_goon", "csb_cletus", "csb_cop", "csb_customer"},
	{"csb_denise_friend", "csb_fos_rep", "csb_g", "csb_groom", "csb_grove_str_dlr", "csb_hao", "csb_hugh", "csb_imran", "csb_janitor", "csb_maude"},
	{"csb_mweather", "csb_ortega", "csb_oscar", "csb_porndudes", "csb_porndudes_p", "csb_prologuedriver", "csb_prolsec", "csb_ramp_gang", "csb_ramp_hic", "csb_ramp_hipster"},
	{"csb_ramp_marine", "csb_ramp_mex", "csb_reporter", "csb_roccopelosi", "csb_screen_writer", "csb_stripper_01", "csb_stripper_02", "csb_tonya", "csb_trafficwarden", "cs_amandatownley"},
	{"cs_andreas", "cs_ashley", "cs_bankman", "cs_barry", "cs_barry_p", "cs_beverly", "cs_beverly_p", "cs_brad", "cs_bradcadaver", "cs_carbuyer"},
	{"cs_casey", "cs_chengsr", "cs_chrisformage", "cs_clay", "cs_dale", "cs_davenorton", "cs_debra", "cs_denise", "cs_devin", "cs_dom"},
	{"cs_dreyfuss", "cs_drfriedlander", "cs_fabien", "cs_fbisuit_01", "cs_floyd", "cs_guadalope", "cs_gurk", "cs_hunter", "cs_janet", "cs_jewelass"},
	{"cs_jimmyboston", "cs_jimmydisanto", "cs_joeminuteman", "cs_johnnyklebitz", "cs_josef", "cs_josh", "cs_lamardavis", "cs_lazlow", "cs_lestercrest", "cs_lifeinvad_01"},
	{"cs_magenta", "cs_manuel", "cs_marnie", "cs_martinmadrazo", "cs_maryann", "cs_michelle", "cs_milton", "cs_molly", "cs_movpremf_01", "cs_movpremmale"},
	{"cs_mrk", "cs_mrsphillips", "cs_mrs_thornhill", "cs_natalia", "cs_nervousron", "cs_nigel", "cs_old_man1a", "cs_old_man2", "cs_omega", "cs_orleans"},
	{"cs_paper", "cs_paper_p", "cs_patricia", "cs_priest", "cs_prolsec_02", "cs_russiandrunk", "cs_siemonyetarian", "cs_solomon", "cs_stevehains", "cs_stretch"},
	{"cs_tanisha", "cs_taocheng", "cs_taostranslator", "cs_tenniscoach", "cs_terry", "cs_tom", "cs_tomepsilon", "cs_tracydisanto", "cs_wade", "cs_zimbor"},
	{"g_f_y_ballas_01", "g_f_y_families_01", "g_f_y_lost_01", "g_f_y_vagos_01", "g_m_m_armboss_01", "g_m_m_armgoon_01", "g_m_m_armlieut_01", "g_m_m_chemwork_01", "g_m_m_chemwork_01_p", "g_m_m_chiboss_01"},
	{"g_m_m_chiboss_01_p", "g_m_m_chicold_01", "g_m_m_chicold_01_p", "g_m_m_chigoon_01", "g_m_m_chigoon_01_p", "g_m_m_chigoon_02", "g_m_m_korboss_01", "g_m_m_mexboss_01", "g_m_m_mexboss_02", "g_m_y_armgoon_02"},
	{"g_m_y_azteca_01", "g_m_y_ballaeast_01", "g_m_y_ballaorig_01", "g_m_y_ballasout_01", "g_m_y_famca_01", "g_m_y_famdnf_01", "g_m_y_famfor_01", "g_m_y_korean_01", "g_m_y_korean_02", "g_m_y_korlieut_01"},
	{"g_m_y_lost_01", "g_m_y_lost_02", "g_m_y_lost_03", "g_m_y_mexgang_01", "g_m_y_mexgoon_01", "g_m_y_mexgoon_02", "g_m_y_mexgoon_03", "g_m_y_mexgoon_03_p", "g_m_y_pologoon_01", "g_m_y_pologoon_01_p"},
	{"g_m_y_pologoon_02", "g_m_y_pologoon_02_p", "g_m_y_salvaboss_01", "g_m_y_salvagoon_01", "g_m_y_salvagoon_02", "g_m_y_salvagoon_03", "g_m_y_salvagoon_03_p", "g_m_y_strpunk_01", "g_m_y_strpunk_02", "hc_driver"},
	{"hc_gunman", "hc_hacker", "ig_abigail", "ig_amandatownley", "ig_andreas", "ig_ashley", "ig_ballasog", "ig_bankman", "ig_barry", "ig_barry_p"},
	{"ig_bestmen", "ig_beverly", "ig_beverly_p", "ig_brad", "ig_bride", "ig_car3guy1", "ig_car3guy2", "ig_casey", "ig_chef", "ig_chengsr"},
	{"ig_chrisformage", "ig_clay", "ig_claypain", "ig_cletus", "ig_dale", "ig_davenorton", "ig_denise", "ig_devin", "ig_dom", "ig_dreyfuss"},
	{"ig_drfriedlander", "ig_fabien", "ig_fbisuit_01", "ig_floyd", "ig_groom", "ig_hao", "ig_hunter", "ig_janet", "ig_jay_norris", "ig_jewelass"},
	{"ig_jimmyboston", "ig_jimmydisanto", "ig_joeminuteman", "ig_johnnyklebitz", "ig_josef", "ig_josh", "ig_kerrymcintosh", "ig_lamardavis", "ig_lazlow", "ig_lestercrest"},
	{"ig_lifeinvad_01", "ig_lifeinvad_02", "ig_magenta", "ig_manuel", "ig_marnie", "ig_maryann", "ig_maude", "ig_michelle", "ig_milton", "ig_molly"},
	{"ig_mrk", "ig_mrsphillips", "ig_mrs_thornhill", "ig_natalia", "ig_nervousron", "ig_nigel", "ig_old_man1a", "ig_old_man2", "ig_omega", "ig_oneil"},
	{"ig_orleans", "ig_ortega", "ig_paper", "ig_patricia", "ig_priest", "ig_prolsec_02", "ig_ramp_gang", "ig_ramp_hic", "ig_ramp_hipster", "ig_ramp_mex"},
	{"ig_roccopelosi", "ig_russiandrunk", "ig_screen_writer", "ig_siemonyetarian", "ig_solomon", "ig_stevehains", "ig_stretch", "ig_talina", "ig_tanisha", "ig_taocheng"},
	{"ig_taostranslator", "ig_taostranslator_p", "ig_tenniscoach", "ig_terry", "ig_tomepsilon", "ig_tonya", "ig_tracydisanto", "ig_trafficwarden", "ig_tylerdix", "ig_wade"},
	{"ig_zimbor", "mp_f_deadhooker", "mp_f_freemode_01", "mp_f_misty_01", "mp_f_stripperlite", "mp_g_m_pros_01", "mp_headtargets", "mp_m_claude_01", "mp_m_exarmy_01", "mp_m_famdd_01"},
	{"mp_m_fibsec_01", "mp_m_freemode_01", "mp_m_marston_01", "mp_m_niko_01", "mp_m_shopkeep_01", "mp_s_m_armoured_01", "", "", "", ""},
	{"", "s_f_m_fembarber", "s_f_m_maid_01", "s_f_m_shop_high", "s_f_m_sweatshop_01", "s_f_y_airhostess_01", "s_f_y_bartender_01", "s_f_y_baywatch_01", "s_f_y_cop_01", "s_f_y_factory_01"},
	{"s_f_y_hooker_01", "s_f_y_hooker_02", "s_f_y_hooker_03", "s_f_y_migrant_01", "s_f_y_movprem_01", "s_f_y_ranger_01", "s_f_y_scrubs_01", "s_f_y_sheriff_01", "s_f_y_shop_low", "s_f_y_shop_mid"},
	{"s_f_y_stripperlite", "s_f_y_stripper_01", "s_f_y_stripper_02", "s_f_y_sweatshop_01", "s_m_m_ammucountry", "s_m_m_armoured_01", "s_m_m_armoured_02", "s_m_m_autoshop_01", "s_m_m_autoshop_02", "s_m_m_bouncer_01"},
	{"s_m_m_chemsec_01", "s_m_m_ciasec_01", "s_m_m_cntrybar_01", "s_m_m_dockwork_01", "s_m_m_doctor_01", "s_m_m_fiboffice_01", "s_m_m_fiboffice_02", "s_m_m_gaffer_01", "s_m_m_gardener_01", "s_m_m_gentransport"},
	{"s_m_m_hairdress_01", "s_m_m_highsec_01", "s_m_m_highsec_02", "s_m_m_janitor", "s_m_m_lathandy_01", "s_m_m_lifeinvad_01", "s_m_m_linecook", "s_m_m_lsmetro_01", "s_m_m_mariachi_01", "s_m_m_marine_01"},
	{"s_m_m_marine_02", "s_m_m_migrant_01", "u_m_y_zombie_01", "s_m_m_movprem_01", "s_m_m_movspace_01", "s_m_m_paramedic_01", "s_m_m_pilot_01", "s_m_m_pilot_02", "s_m_m_postal_01", "s_m_m_postal_02"},
	{"s_m_m_prisguard_01", "s_m_m_scientist_01", "s_m_m_security_01", "s_m_m_snowcop_01", "s_m_m_strperf_01", "s_m_m_strpreach_01", "s_m_m_strvend_01", "s_m_m_trucker_01", "s_m_m_ups_01", "s_m_m_ups_02"},
	{"s_m_o_busker_01", "s_m_y_airworker", "s_m_y_ammucity_01", "s_m_y_armymech_01", "s_m_y_autopsy_01", "s_m_y_barman_01", "s_m_y_baywatch_01", "s_m_y_blackops_01", "s_m_y_blackops_02", "s_m_y_busboy_01"},
	{"s_m_y_chef_01", "s_m_y_clown_01", "s_m_y_construct_01", "s_m_y_construct_02", "s_m_y_cop_01", "s_m_y_dealer_01", "s_m_y_devinsec_01", "s_m_y_dockwork_01", "s_m_y_doorman_01", "s_m_y_dwservice_01"},
	{"s_m_y_dwservice_02", "s_m_y_factory_01", "s_m_y_fireman_01", "s_m_y_garbage", "s_m_y_grip_01", "s_m_y_hwaycop_01", "s_m_y_marine_01", "s_m_y_marine_02", "s_m_y_marine_03", "s_m_y_mime"},
	{"s_m_y_pestcont_01", "s_m_y_pilot_01", "s_m_y_prismuscl_01", "s_m_y_prisoner_01", "s_m_y_ranger_01", "s_m_y_robber_01", "s_m_y_sheriff_01", "s_m_y_shop_mask", "s_m_y_strvend_01", "s_m_y_swat_01"},
	{"s_m_y_uscg_01", "s_m_y_valet_01", "s_m_y_waiter_01", "s_m_y_winclean_01", "s_m_y_xmech_01", "s_m_y_xmech_02", "u_f_m_corpse_01", "u_f_m_miranda", "u_f_m_promourn_01", "u_f_o_moviestar"},
	{"u_f_o_prolhost_01", "u_f_y_bikerchic", "u_f_y_comjane", "u_f_y_corpse_01", "u_f_y_corpse_02", "u_f_y_hotposh_01", "u_f_y_jewelass_01", "u_f_y_mistress", "u_f_y_poppymich", "u_f_y_princess"},
	{"u_f_y_spyactress", "u_m_m_aldinapoli", "u_m_m_bankman", "u_m_m_bikehire_01", "u_m_m_fibarchitect", "u_m_m_filmdirector", "u_m_m_glenstank_01", "u_m_m_griff_01", "u_m_m_jesus_01", "u_m_m_jewelsec_01"},
	{"u_m_m_jewelthief", "u_m_m_markfost", "u_m_m_partytarget", "u_m_m_prolsec_01", "u_m_m_promourn_01", "u_m_m_rivalpap", "u_m_m_spyactor", "u_m_m_willyfist", "u_m_o_finguru_01", "u_m_o_taphillbilly"},
	{"u_m_o_tramp_01", "u_m_y_abner", "u_m_y_antonb", "u_m_y_babyd", "u_m_y_baygor", "u_m_y_burgerdrug_01", "u_m_y_chip", "u_m_y_cyclist_01", "u_m_y_fibmugger_01", "u_m_y_guido_01"},
	{"u_m_y_gunvend_01", "u_m_y_hippie_01", "u_m_y_imporage", "u_m_y_justin", "u_m_y_mani", "u_m_y_militarybum", "u_m_y_paparazzi", "u_m_y_party_01", "u_m_y_pogo_01", "u_m_y_prisoner_01"},
	{"ig_benny", "ig_g", "ig_vagspeak", "mp_m_g_vagfun_01", "mp_m_boatstaff_01", "mp_f_boatstaff_01", "", "", "", ""}
};

LPCSTR pedModelNames[70][10] = {
	{"MICHAEL", "FRANKLIN", "TREVOR", "BOAR", "CHIMP", "COW", "COYOTE", "DEER", "FISH", "HEN"},
	{ "CAT", "HAWK", "CORMORANT", "CROW", "DOLPHIN", "HUMPBACK", "WHALE", "PIGEON", "SEAGULL", "SHARKHAMMER" },
	{"PIG", "RAT", "RHESUS", "CHOP", "HUSKY", "MTLION", "RETRIEVER", "SHARKTIGER", "SHEPHERD", "ALIEN"},
	{"BEACH", "BEVHILLS", "BEVHILLS", "BODYBUILD", "BUSINESS", "DOWNTOWN", "EASTSA", "EASTSA", "FATBLA", "FATCULT"},
	{"FATWHITE", "KTOWN", "KTOWN", "PROLHOST", "SALTON", "SKIDROW", "SOUCENTMC", "SOUCENT", "SOUCENT", "TOURIST"},
	{"TRAMPBEAC", "TRAMP", "GENSTREET", "INDIAN", "KTOWN", "SALTON", "SOUCENT", "SOUCENT", "BEACH", "BEVHILLS"},
	{"BEVHILLS", "BEVHILLS", "BEVHILLS", "BUSINESS", "BUSINESS", "BUSINESS", "BUSINESS", "EASTSA", "EASTSA", "EASTSA"},
	{"EPSILON", "FITNESS", "FITNESS", "GENHOT", "GOLFER", "HIKER", "HIPPIE", "HIPSTER", "HIPSTER", "HIPSTER"},
	{"HIPSTER", "INDIAN", "JUGGALO", "RUNNER", "RURMETH", "SCDRESSY", "SKATER", "SOUCENT", "SOUCENT", "SOUCENT"},
	{"TENNIS", "TOPLESS", "TOURIST", "TOURIST", "VINEWOOD", "VINEWOOD", "VINEWOOD", "VINEWOOD", "YOGA", "ACULT"},
	{"AFRIAMER", "BEACH", "BEACH", "BEVHILLS", "BEVHILLS", "BUSINESS", "EASTSA", "EASTSA", "FARMER", "FATLATIN"},
	{"GENFAT", "GENFAT", "GOLFER", "HASJEW", "HILLBILLY", "HILLBILLY", "INDIAN", "KTOWN", "MALIBU", "MEXCNTRY"},
	{"MEXLABOR", "OG_BOSS", "PAPARAZZI", "POLYNESIAN", "PROLHOST", "RURMETH", "SALTON", "SALTON", "SALTON", "SALTON"},
	{"SKATER", "SKIDROW", "SOCENLAT", "SOUCENT", "SOUCENT", "SOUCENT", "SOUCENT", "STLAT", "TENNIS", "TOURIST"},
	{"TRAMPBEAC", "TRAMP", "TRANVEST", "TRANVEST", "ACULT", "ACULT", "BEACH", "GENSTREET", "KTOWN", "SALTON"},
	{"SOUCENT", "SOUCENT", "SOUCENT", "TRAMP", "ACULT", "ACULT", "BEACHVESP", "BEACHVESP", "BEACH", "BEACH"},
	{"BEACH", "BEVHILLS", "BEVHILLS", "BREAKDANCE", "BUSICAS", "BUSINESS", "BUSINESS", "BUSINESS", "CYCLIST", "DHILL"},
	{"DOWNTOWN", "EASTSA", "EASTSA", "EPSILON", "EPSILON", "GAY", "GAY", "GENSTREET", "GENSTREET", "GOLFER"},
	{"HASJEW", "HIKER", "HIPPY", "HIPSTER", "HIPSTER", "HIPSTER", "INDIAN", "JETSKI", "JUGGALO", "KTOWN"},
	{"KTOWN", "LATINO", "METHHEAD", "MEXTHUG", "MOTOX", "MOTOX", "MUSCLBEAC", "MUSCLBEAC", "POLYNESIAN", "ROADCYC"},
	{"RUNNER", "RUNNER", "SALTON", "SKATER", "SKATER", "SOUCENT", "SOUCENT", "SOUCENT", "SOUCENT", "STBLA"},
	{"STBLA", "STLAT", "STWHI", "STWHI", "SUNBATHE", "SURFER", "VINDOUCHE", "VINEWOOD", "VINEWOOD", "VINEWOOD"},
	{"VINEWOOD", "YOGA", "PROLDRIVER", "RSRANGER", "SBIKE", "STAGGRM", "TATTOO", "ABIGAIL", "ANITA", "ANTON"},
	{"BALLASOG", "BRIDE", "BURGERDRUG", "CAR3GUY1", "CAR3GUY2", "CHEF", "CHIN_GOON", "CLETUS", "COP", "CUSTOMER"},
	{"DENISE_FRIEND", "FOS_REP", "G", "GROOM", "DLR", "HAO", "HUGH", "IMRAN", "JANITOR", "MAUDE"},
	{"MWEATHER", "ORTEGA", "OSCAR", "PORNDUDES", "PORNDUDES_P", "PROLOGUEDRIVER", "PROLSEC", "GANG", "HIC", "HIPSTER"},
	{"MARINE", "MEX", "REPORTER", "ROCCOPELOSI", "SCREEN_WRITER", "STRIPPER", "STRIPPER", "TONYA", "TRAFFICWARDEN", "AMANDATOWNLEY"},
	{"ANDREAS", "ASHLEY", "BANKMAN", "BARRY", "BARRY_P", "BEVERLY", "BEVERLY_P", "BRAD", "BRADCADAVER", "CARBUYER"},
	{"CASEY", "CHENGSR", "CHRISFORMAGE", "CLAY", "DALE", "DAVENORTON", "DEBRA", "DENISE", "DEVIN", "DOM"},
	{"DREYFUSS", "DRFRIEDLANDER", "FABIEN", "FBISUIT", "FLOYD", "GUADALOPE", "GURK", "HUNTER", "JANET", "JEWELASS"},
	{"JIMMYBOSTON", "JIMMYDISANTO", "JOEMINUTEMAN", "JOHNNYKLEBITZ", "JOSEF", "JOSH", "LAMARDAVIS", "LAZLOW", "LESTERCREST", "LIFEINVAD"},
	{"MAGENTA", "MANUEL", "MARNIE", "MARTINMADRAZO", "MARYANN", "MICHELLE", "MILTON", "MOLLY", "MOVPREMF", "MOVPREMMALE"},
	{"MRK", "MRSPHILLIPS", "MRS_THORNHILL", "NATALIA", "NERVOUSRON", "NIGEL", "OLD_MAN1A", "OLD_MAN2", "OMEGA", "ORLEANS"},
	{"PAPER", "PAPER_P", "PATRICIA", "PRIEST", "PROLSEC", "RUSSIANDRUNK", "SIEMONYETARIAN", "SOLOMON", "STEVEHAINS", "STRETCH"},
	{"TANISHA", "TAOCHENG", "TAOSTRANSLATOR", "TENNISCOACH", "TERRY", "TOM", "TOMEPSILON", "TRACYDISANTO", "WADE", "ZIMBOR"},
	{"BALLAS", "FAMILIES", "LOST", "VAGOS", "ARMBOSS", "ARMGOON", "ARMLIEUT", "CHEMWORK", "CHEMWORK_P", "CHIBOSS"},
	{"CHIBOSS_P", "CHICOLD", "CHICOLD_P", "CHIGOON", "CHIGOON_P", "CHIGOON", "KORBOSS", "MEXBOSS", "MEXBOSS", "ARMGOON"},
	{"AZTECA", "BALLAEAST", "BALLAORIG", "BALLASOUT", "FAMCA", "FAMDNF", "FAMFOR", "KOREAN", "KOREAN", "KORLIEUT"},
	{"LOST", "LOST", "LOST", "MEXGANG", "MEXGOON", "MEXGOON", "MEXGOON", "MEXGOON_P", "POLOGOON", "POLOGOON_P"},
	{"POLOGOON", "POLOGOON_P", "SALVABOSS", "SALVAGOON", "SALVAGOON", "SALVAGOON", "SALVAGOON_P", "STRPUNK", "STRPUNK", "HC_DRIVER"},
	{"HC_GUNMAN", "HC_HACKER", "ABIGAIL", "AMANDATOWNLEY", "ANDREAS", "ASHLEY", "BALLASOG", "BANKMAN", "BARRY", "BARRY_P"},
	{"BESTMEN", "BEVERLY", "BEVERLY_P", "BRAD", "BRIDE", "CAR3GUY1", "CAR3GUY2", "CASEY", "CHEF", "CHENGSR"},
	{"CHRISFORMAGE", "CLAY", "CLAYPAIN", "CLETUS", "DALE", "DAVENORTON", "DENISE", "DEVIN", "DOM", "DREYFUSS"},
	{"DRFRIEDLANDER", "FABIEN", "FBISUIT", "FLOYD", "GROOM", "HAO", "HUNTER", "JANET", "JAY_NORRIS", "JEWELASS"},
	{"JIMMYBOSTON", "JIMMYDISANTO", "JOEMINUTEMAN", "JOHNNYKLEBITZ", "JOSEF", "JOSH", "KERRYMCINTOSH", "LAMARDAVIS", "LAZLOW", "LESTERCREST"},
	{"LIFEINVAD", "LIFEINVAD", "MAGENTA", "MANUEL", "MARNIE", "MARYANN", "MAUDE", "MICHELLE", "MILTON", "MOLLY"},
	{"MRK", "MRSPHILLIPS", "MRS_THORNHILL", "NATALIA", "NERVOUSRON", "NIGEL", "OLD_MAN1A", "OLD_MAN2", "OMEGA", "ONEIL"},
	{"ORLEANS", "ORTEGA", "PAPER", "PATRICIA", "PRIEST", "PROLSEC", "GANG", "HIC", "HIPSTER", "MEX"},
	{"ROCCOPELOSI", "RUSSIANDRUNK", "SCREEN_WRITER", "SIEMONYETARIAN", "SOLOMON", "STEVEHAINS", "STRETCH", "TALINA", "TANISHA", "TAOCHENG"},
	{"TAOSTRANSLATOR", "TAOSTRANSLATOR_P", "TENNISCOACH", "TERRY", "TOMEPSILON", "TONYA", "TRACYDISANTO", "TRAFFICWARDEN", "TYLERDIX", "WADE"},
	{"ZIMBOR", "DEADHOOKER", "FREEMODE", "MISTY", "STRIPPERLITE", "PROS", "MP_HEADTARGETS", "CLAUDE", "EXARMY", "FAMDD"},
	{"FIBSEC", "FREEMODE", "MARSTON", "NIKO", "SHOPKEEP", "ARMOURED", "NONE", "NONE", "NONE", "NONE"},
	{"NONE", "FEMBARBER", "MAID", "SHOP_HIGH", "SWEATSHOP", "AIRHOSTESS", "BARTENDER", "BAYWATCH", "COP", "FACTORY"},
	{"HOOKER", "HOOKER", "HOOKER", "MIGRANT", "MOVPREM", "RANGER", "SCRUBS", "SHERIFF", "SHOP_LOW", "SHOP_MID"},
	{"STRIPPERLITE", "STRIPPER", "STRIPPER", "SWEATSHOP", "AMMUCOUNTRY", "ARMOURED", "ARMOURED", "AUTOSHOP", "AUTOSHOP", "BOUNCER"},
	{"CHEMSEC", "CIASEC", "CNTRYBAR", "DOCKWORK", "DOCTOR", "FIBOFFICE", "FIBOFFICE", "GAFFER", "GARDENER", "GENTRANSPORT"},
	{"HAIRDRESS", "HIGHSEC", "HIGHSEC", "JANITOR", "LATHANDY", "LIFEINVAD", "LINECOOK", "LSMETRO", "MARIACHI", "MARINE"},
	{"MARINE", "MIGRANT", "ZOMBIE", "MOVPREM", "MOVSPACE", "PARAMEDIC", "PILOT", "PILOT", "POSTAL", "POSTAL"},
	{"PRISGUARD", "SCIENTIST", "SECURITY", "SNOWCOP", "STRPERF", "STRPREACH", "STRVEND", "TRUCKER", "UPS", "UPS"},
	{"BUSKER", "AIRWORKER", "AMMUCITY", "ARMYMECH", "AUTOPSY", "BARMAN", "BAYWATCH", "BLACKOPS", "BLACKOPS", "BUSBOY"},
	{"CHEF", "CLOWN", "CONSTRUCT", "CONSTRUCT", "COP", "DEALER", "DEVINSEC", "DOCKWORK", "DOORMAN", "DWSERVICE"},
	{"DWSERVICE", "FACTORY", "FIREMAN", "GARBAGE", "GRIP", "HWAYCOP", "MARINE", "MARINE", "MARINE", "MIME"},
	{"PESTCONT", "PILOT", "PRISMUSCL", "PRISONER", "RANGER", "ROBBER", "SHERIFF", "SHOP_MASK", "STRVEND", "SWAT"},
	{"USCG", "VALET", "WAITER", "WINCLEAN", "XMECH", "XMECH", "CORPSE", "MIRANDA", "PROMOURN", "MOVIESTAR"},
	{"PROLHOST", "BIKERCHIC", "COMJANE", "CORPSE", "CORPSE", "HOTPOSH", "JEWELASS", "MISTRESS", "POPPYMICH", "PRINCESS"},
	{"SPYACTRESS", "ALDINAPOLI", "BANKMAN", "BIKEHIRE", "FIBARCHITECT", "FILMDIRECTOR", "GLENSTANK", "GRIFF", "JESUS", "JEWELSEC"},
	{"JEWELTHIEF", "MARKFOST", "PARTYTARGET", "PROLSEC", "PROMOURN", "RIVALPAP", "SPYACTOR", "WILLYFIST", "FINGURU", "TAPHILLBILLY"},
	{"TRAMP", "ABNER", "ANTONB", "BABYD", "BAYGOR", "BURGERDRUG", "CHIP", "CYCLIST", "FIBMUGGER", "GUIDO"},
	{"GUNVEND", "HIPPIE", "IMPORAGE", "JUSTIN", "MANI", "MILITARYBUM", "PAPARAZZI", "PARTY", "POGO", "PRISONER"},
	{"BENNY", "G", "VAGSPEAK", "VAGFUN", "BOATSTAFF", "FEMBOATSTAFF", "", "", "", ""}
};

int skinchangerActiveLineIndex = 0;
int skinchangerActiveItemIndex = 0;

bool process_skinchanger_menu()
{
	DWORD waitTime = 150;
	const int lineCount = 70;
	const int itemCount = 10;
	const int itemCountLastLine = itemCount;//6;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do 
		{
			// draw menu
			char caption[32];
			sprintf_s(caption, "Skin changer   %d / %d", skinchangerActiveLineIndex + 1, lineCount);
			draw_menu_line(caption, 350.0, 15.0, 18.0, 0.0, 5.0, false, true);
			for (int i = 0; i < itemCount; i++)
				if (strlen(pedModels[skinchangerActiveLineIndex][i]) || strcmp(pedModelNames[skinchangerActiveLineIndex][i], "NONE") == 0)
					draw_menu_line(pedModelNames[skinchangerActiveLineIndex][i], 100.0f, 5.0f, 200.0f, 100.0f + i * 110.0f, 5.0f, i == skinchangerActiveItemIndex, false, false);
			
			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		bool bSelect, bBack, bUp, bDown, bLeft, bRight;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, &bLeft, &bRight);
		
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			DWORD model = $((char *)pedModels[skinchangerActiveLineIndex][skinchangerActiveItemIndex]);
			if (STREAMING::IS_MODEL_IN_CDIMAGE(model) && STREAMING::IS_MODEL_VALID(model))
			{
				STREAMING::REQUEST_MENU_PED_MODEL(model);
				while (!STREAMING::HAS_MODEL_LOADED(model))	WAIT(0); 
				//STREAMING::LOAD_ALL_OBJECTS_NOW();
				PLAYER::SET_PLAYER_MODEL(PLAYER::PLAYER_ID(), model);
				//PED::SET_PED_RANDOM_COMPONENT_VARIATION(PLAYER::PLAYER_PED_ID(), FALSE);
				PED::SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER::PLAYER_PED_ID());				
				WAIT(0);
				for (int i = 0; i < 12; i++)
					for (int j = 0; j < 100; j++)
					{
						int drawable = rand() % 10;
						int texture = rand() % 10;
						if (PED::IS_PED_COMPONENT_VARIATION_VALID(PLAYER::PLAYER_PED_ID(), i, drawable, texture))
						{
							PED::SET_PED_COMPONENT_VARIATION(PLAYER::PLAYER_PED_ID(), i, drawable, texture, 0);
							break;
						}
					}
				skinchanger_used = true;
				WAIT(100);
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);				
				waitTime = 200;
			}
		} else
		if (bBack)
		{
			menu_beep(NAV_CANCEL);
			break;
		} else
		if (bRight)
		{
			menu_beep(NAV_LEFT_RIGHT);
			skinchangerActiveItemIndex++;
			int itemsMax = (skinchangerActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
			if (skinchangerActiveItemIndex == itemsMax) 
				skinchangerActiveItemIndex = 0;			
			waitTime = 100;
		} else
		if (bLeft)
		{
			menu_beep(NAV_LEFT_RIGHT);
			if (skinchangerActiveItemIndex == 0) 
				skinchangerActiveItemIndex = (skinchangerActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
			skinchangerActiveItemIndex--;
			waitTime = 100;
		} else
		if (bUp)
		{
			menu_beep(NAV_UP_DOWN);
			if (skinchangerActiveLineIndex == 0) 
				skinchangerActiveLineIndex = lineCount;
			skinchangerActiveLineIndex--;
			waitTime = 200;
		} else
		if (bDown)
		{
			menu_beep(NAV_UP_DOWN);
			skinchangerActiveLineIndex++;
			if (skinchangerActiveLineIndex == lineCount) 
				skinchangerActiveLineIndex = 0;			
			waitTime = 200;
		}
		if (skinchangerActiveLineIndex == (lineCount - 1))
			if (skinchangerActiveItemIndex >= itemCountLastLine)
				skinchangerActiveItemIndex = 0;
	}
	return false;
}

int teleportActiveLineIndex = 0;
bool process_teleport_menu() {
	const float LineWidth = 250.;
	const int LineCount = 17;
	std::string Caption = "Teleport  Options";
	static struct {
		LPCSTR Text;
		float X;
		float Y;
		float Z;
	}lines[LineCount] = {
		{ "Waypoint" },
		{ "Michael's House",-852.4f,160.0f,65.6f },
		{ "Franklin's House",7.9f,548.1f,175.5f },
		{ "Trevor's Trailer",1985.7f,3812.2f,32.2f },
		{ "Airport Entrance",-1034.6f,-2733.6f,13.8f },
		{ "Airport Field",-1336.0f,-3044.0f,13.9f },
		{ "Elysian Island",338.2f,-2715.9f,38.5f },
		{ "Jetsam",760.4f,-2943.2f,5.8f },
		{ "Stripclub",127.4f,-1307.7f,29.2f },
		{ "Elburro Heights",1384.0f,-2057.1f,52.0f },
		{ "Ferris Wheel",-1670.7f,-1125.0f,13.0f },
		{ "Chumash",-3192.6f,1100.0f,20.2f },
		{ "Windfarm",2354.0f,1830.3f,101.1f },
		{ "Military Base",-2047.4f,3132.1f,32.8f },
		{ "McKenzie Airfield",2121.7f,4796.3f,41.1f },
		{ "Desert Airfield",1747.0f,3273.7f,41.1f },
		{ "Chilliad",425.4f,5614.3f,766.5f } };
	DWORD WaitTime = 150;
	while (true) {
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + WaitTime;
		do {
			// draw menu
			draw_menu_line(Caption, LineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int I = 0; I<LineCount; I++)
				if (I != teleportActiveLineIndex)
					draw_menu_line(lines[I].Text, LineWidth, 9.0f, 60.0f + I*30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(lines[teleportActiveLineIndex].Text,
				LineWidth + 0.0f, 9.0f, 60.0f + teleportActiveLineIndex*30.0f, 1000.0f, 7.0f, true, false);
			update_features();
			WAIT(0);
		} while (GetTickCount()<maxTickCount);
		WaitTime = 0;
		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect) {
			menu_beep(NAV_SELECT);
			// get entity to teleport
			Entity E = PLAYER::PLAYER_PED_ID();
			if (PED::IS_PED_IN_ANY_VEHICLE(E, 0))
				E = PED::GET_VEHICLE_PED_IS_USING(E);
			// get coords
			Vector3 Coordinates;
			bool Success = false;
			if (teleportActiveLineIndex == 0) {
				bool BlipFound = false;
				// search for marker blip
				int BlipIterator = UI::_GET_BLIP_INFO_ID_ITERATOR();
				for (Blip I = UI::GET_FIRST_BLIP_INFO_ID(BlipIterator); UI::DOES_BLIP_EXIST(I) != 0; I = UI::GET_NEXT_BLIP_INFO_ID(BlipIterator)) {
					if (UI::GET_BLIP_INFO_ID_TYPE(I) == 4) {
						Coordinates = UI::GET_BLIP_INFO_ID_COORD(I);
						BlipFound = true;
						break;
					}
				}
				if (BlipFound) {
					// load needed map region and check height levels for ground existence
					bool GroundFound = false;
					static float GroundCheckHeight[] = { 100.0,150.0,50.0,0.0,200.0,250.0,300.0,350.0,400.0,
						450.0,500.0,550.0,600.0,650.0,700.0,750.0,800.0 };
					for (int I = 0; I< sizeof(GroundCheckHeight) / sizeof(float); I++) {
						ENTITY::SET_ENTITY_COORDS_NO_OFFSET(E, Coordinates.x, Coordinates.y, GroundCheckHeight[I], 0, 0, 1);
						WAIT(100);
						if (GAMEPLAY::GET_GROUND_Z_FOR_3D_COORD(Coordinates.x, Coordinates.y, GroundCheckHeight[I], &Coordinates.z, 0)) {
							GroundFound = true;
							Coordinates.z += 3.0;
							break;
						}
					}
					// if ground not found then set Z in air and give player a parachute
					if (!GroundFound) {
						Coordinates.z = 1000.0;
						WEAPON::GIVE_DELAYED_WEAPON_TO_PED(PLAYER::PLAYER_PED_ID(), 0xFBAB5776, 1, 0);
					}
					Success = true;
				}
				else {}
			}
			else {
				Coordinates.x = lines[teleportActiveLineIndex].X;
				Coordinates.y = lines[teleportActiveLineIndex].Y;
				Coordinates.z = lines[teleportActiveLineIndex].Z;
				Success = true;
			}
			// set player pos
			if (Success) {
				ENTITY::SET_ENTITY_COORDS_NO_OFFSET(E, Coordinates.x, Coordinates.y, Coordinates.z, 0, 0, 1);
				WAIT(0);
				set_status_text("~HUD_COLOUR_GREEN~TELEPORT COMPLETE");
				return true;
			}
			WaitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed()) {
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp) {
					menu_beep(NAV_UP_DOWN);
					if (teleportActiveLineIndex == 0)
						teleportActiveLineIndex = LineCount;
					teleportActiveLineIndex--;
					WaitTime = 150;
				}
				else
					if (bDown) {
						menu_beep(NAV_UP_DOWN);
						teleportActiveLineIndex++;
						if (teleportActiveLineIndex == LineCount)
							teleportActiveLineIndex = 0;
						WaitTime = 150;
					}
	}
	return false;
}

std::string line_as_str(std::string text, bool*pState) {


	while (text.size()<18)text += " ";

	return text + (pState ? (*pState ? "~HUD_COLOUR_GREEN~  ON" : "~HUD_COLOUR_RED~  OFF") : "");
}

int activeLineIndexPlayer = 0;
void process_player_menu() {
	const float lineWidth = 250.0;
	const int lineCount = 16;

	std::string caption = "Player  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Skin Selector ~HUD_COLOUR_BLUE~>", NULL, NULL },
		{ "Fix Player ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Restore Skin ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Singleplayer Cash ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Remove Blood Stains ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Wanted Level Increase ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Wanted Level Decrease ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Never Wanted ~HUD_COLOUR_GREEN~:", &featurePlayerNeverWanted, NULL },
		{ "Invincible ~HUD_COLOUR_GREEN~:", &featurePlayerInvincible, &featurePlayerInvincibleUpdated },
		{ "Invisible ~HUD_COLOUR_GREEN~:",&featurePlayerInvisible ,NULL },
		{ "Police Ignore You ~HUD_COLOUR_GREEN~:", &featurePlayerIgnored, &featurePlayerIgnoredUpdated },
		{ "Unlimited Abilities ~HUD_COLOUR_GREEN~:", &featurePlayerUnlimitedAbility, NULL },
		{ "Noiseless ~HUD_COLOUR_GREEN~:", &featurePlayerNoNoise, &featurePlayerNoNoiseUpdated },
		{ "Fast Swim ~HUD_COLOUR_GREEN~:", &featurePlayerFastSwim, &featurePlayerFastSwimUpdated },
		{ "Fast Run ~HUD_COLOUR_GREEN~:", &featurePlayerFastRun, &featurePlayerFastRunUpdated },
		{ "Super Jump ~HUD_COLOUR_GREEN~:", &featurePlayerSuperJump, NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexPlayer)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexPlayer].text, lines[activeLineIndexPlayer].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexPlayer * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID());
			Player player = PLAYER::PLAYER_ID();
			Ped playerPed = PLAYER::PLAYER_PED_ID();
			switch (activeLineIndexPlayer)
			{
				// skin changer
			case 0:
				if (process_skinchanger_menu())	return;
				break;
				// fix player
			case 1:
			{
				ENTITY::SET_ENTITY_HEALTH(playerPed, ENTITY::GET_ENTITY_MAX_HEALTH(playerPed));
				PED::ADD_ARMOUR_TO_PED(playerPed, PLAYER::GET_PLAYER_MAX_ARMOUR(player) - PED::GET_PED_ARMOUR(playerPed));
				if (PED::IS_PED_IN_ANY_VEHICLE(playerPed, 0))
				{
					Vehicle playerVeh = PED::GET_VEHICLE_PED_IS_USING(playerPed);
					if (ENTITY::DOES_ENTITY_EXIST(playerVeh) && !ENTITY::IS_ENTITY_DEAD(playerVeh))
						VEHICLE::SET_VEHICLE_FIXED(playerVeh);
				}
				set_status_text("~HUD_COLOUR_YELLOW~PLAYER RESTORED");
			}
			break;
			// reset model skin
			case 2:
			{
				PED::SET_PED_DEFAULT_COMPONENT_VARIATION(playerPed);
				set_status_text("~HUD_COLOUR_YELLOW~PLAYER RESTORED");
			}
			break;
			// add cash
			case 3:
				for (int i = 0; i < 3; i++)
				{
					char statNameFull[32];
					sprintf_s(statNameFull, "SP%d_TOTAL_CASH", i);
					Hash hash = $(statNameFull);
					int val;
					STATS::STAT_GET_INT(hash, &val, -1);
					val += 1000000;
					STATS::STAT_SET_INT(hash, val, 1);
				}
				set_status_text("~HUD_COLOUR_GREEN~CASH RECEIVED");
				AUDIO::PLAY_SOUND_FRONTEND(-1, "PICK_UP", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0);
				break;
			case 4:
				if (bPlayerExists)
				{
					PED::CLEAR_PED_BLOOD_DAMAGE(playerPed);
				}
				break;
				// wanted up
			case 5:
				if (bPlayerExists && PLAYER::GET_PLAYER_WANTED_LEVEL(player) < 5)
				{
					PLAYER::SET_PLAYER_WANTED_LEVEL(player, PLAYER::GET_PLAYER_WANTED_LEVEL(player) + 1, 0);
					PLAYER::SET_PLAYER_WANTED_LEVEL_NOW(player, 0);
					set_status_text("~HUD_COLOUR_RED~WANTED LEVEL RAISED");
				}
				break;
				GAMEPLAY::SET_FAKE_WANTED_LEVEL(PLAYER::GET_PLAYER_WANTED_LEVEL(player));
				// wanted down
			case 6:
				if (bPlayerExists && PLAYER::GET_PLAYER_WANTED_LEVEL(player) > 0)
				{
					PLAYER::SET_PLAYER_WANTED_LEVEL(player, PLAYER::GET_PLAYER_WANTED_LEVEL(player) - 1, 0);
					PLAYER::SET_PLAYER_WANTED_LEVEL_NOW(player, 0);
					set_status_text("~HUD_COLOUR_GREEN~WANTED LEVEL LOWERED");
				}
				break;
				// switchable features
			default:
				if (lines[activeLineIndexPlayer].pState)
					*lines[activeLineIndexPlayer].pState = !(*lines[activeLineIndexPlayer].pState);
				if (lines[activeLineIndexPlayer].pUpdated)
					*lines[activeLineIndexPlayer].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexPlayer == 0)
						activeLineIndexPlayer = lineCount;
					activeLineIndexPlayer--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexPlayer++;
						if (activeLineIndexPlayer == lineCount)
							activeLineIndexPlayer = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexWeapon = 0;
int activeLineIndexOWeapon = 0;
static LPCSTR weaponNames[] = {
	"WEAPON_KNIFE", "WEAPON_NIGHTSTICK", "WEAPON_HAMMER", "WEAPON_BAT", "WEAPON_GOLFCLUB", "WEAPON_CROWBAR",
	"WEAPON_PISTOL", "WEAPON_COMBATPISTOL", "WEAPON_APPISTOL", "WEAPON_PISTOL50", "WEAPON_MICROSMG", "WEAPON_SMG",
	"WEAPON_ASSAULTSMG", "WEAPON_ASSAULTRIFLE", "WEAPON_CARBINERIFLE", "WEAPON_ADVANCEDRIFLE", "WEAPON_MG",
	"WEAPON_COMBATMG", "WEAPON_PUMPSHOTGUN", "WEAPON_SAWNOFFSHOTGUN", "WEAPON_ASSAULTSHOTGUN", "WEAPON_BULLPUPSHOTGUN",
	"WEAPON_STUNGUN", "WEAPON_SNIPERRIFLE", "WEAPON_HEAVYSNIPER", "WEAPON_GRENADELAUNCHER", "WEAPON_GRENADELAUNCHER_SMOKE",
	"WEAPON_RPG", "WEAPON_MINIGUN", "WEAPON_GRENADE", "WEAPON_STICKYBOMB", "WEAPON_SMOKEGRENADE", "WEAPON_BZGAS",
	"WEAPON_MOLOTOV", "WEAPON_FIREEXTINGUISHER", "WEAPON_PETROLCAN",
	"WEAPON_SNSPISTOL", "WEAPON_SPECIALCARBINE", "WEAPON_HEAVYPISTOL", "WEAPON_BULLPUPRIFLE", "WEAPON_HOMINGLAUNCHER",
	"WEAPON_PROXMINE", "WEAPON_SNOWBALL", "WEAPON_VINTAGEPISTOL", "WEAPON_DAGGER", "WEAPON_FIREWORK", "WEAPON_MUSKET",
	"WEAPON_MARKSMANRIFLE", "WEAPON_HEAVYSHOTGUN", "WEAPON_GUSENBERG", "WEAPON_HATCHET", "WEAPON_RAILGUN",
	"WEAPON_COMBATPDW", "WEAPON_KNUCKLE", "WEAPON_MARKSMANPISTOL"
};

void process_object_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 6;

	std::string caption = "Weapon Ammo Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Shoot Cash ~HUD_COLOUR_GREEN~:",&featureObjectGun, NULL },
		{ "Shoot Armor ~HUD_COLOUR_GREEN~:",&featureObjectGun1, NULL },
		{ "Shoot Health ~HUD_COLOUR_GREEN~:",&featureObjectGun2, NULL },
		{ "Fire Ammo ~HUD_COLOUR_GREEN~:",&featureWeaponFireAmmo,NULL },
		{ "Explosive Ammo ~HUD_COLOUR_GREEN~:",&featureWeaponExplosiveAmmo,NULL },
		{ "Teleport Gun ~HUD_COLOUR_GREEN~:",&featureWeaponTeleportToImpact, NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexOWeapon)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexOWeapon].text, lines[activeLineIndexOWeapon].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexOWeapon * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID());
			Player player = PLAYER::PLAYER_ID();
			Ped playerPed = PLAYER::PLAYER_PED_ID();

			//switch (activeLineIndexOWeapon)
			//{

				// switchable features
			//default:
				if (lines[activeLineIndexOWeapon].pState)
					*lines[activeLineIndexOWeapon].pState = !(*lines[activeLineIndexOWeapon].pState);
				if (lines[activeLineIndexOWeapon].pUpdated)
					*lines[activeLineIndexOWeapon].pUpdated = true;
			//}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexOWeapon == 0)
						activeLineIndexOWeapon = lineCount;
					activeLineIndexOWeapon--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexOWeapon++;
						if (activeLineIndexOWeapon == lineCount)
							activeLineIndexOWeapon = 0;
						waitTime = 150;
					}
	}
}

void process_weapon_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 7;

	std::string caption = "Weapon  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Recieve All Weapons ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Ammo Modifications ~HUD_COLOUR_BLUE~>",NULL, NULL },
		{ "Throw Knife RPG ~HUD_COLOUR_GREEN~:",&featureWeaponPlaRockets, NULL },
		{ "One Hit Kill ~HUD_COLOUR_GREEN~:", &featureWeaponOneHit, NULL },
		{ "No Reload ~HUD_COLOUR_GREEN~:",&featureWeaponNoReload,NULL },
		{ "Explosive Melee ~HUD_COLOUR_GREEN~:",&featureWeaponExplosiveMelee,NULL },
		{ "Vehicle Rockets ~HUD_COLOUR_GREEN~:",&featureWeaponVehRockets,NULL }

	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexWeapon)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexWeapon].text, lines[activeLineIndexWeapon].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexWeapon * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID());
			Player player = PLAYER::PLAYER_ID();
			Ped playerPed = PLAYER::PLAYER_PED_ID();

			switch (activeLineIndexWeapon)
			{
			case 0:
				for (int i = 0; i < sizeof(weaponNames) / sizeof(weaponNames[0]); i++)
					WEAPON::GIVE_DELAYED_WEAPON_TO_PED(playerPed, $((char *)weaponNames[i]), 1000, 0);
				set_status_text("~HUD_COLOUR_GREEN~All Weapons Recieved");
				break;
			case 1:
				set_status_text("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~High-Risk Features!");
				NotifyBottomCentre("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~Shooting Pickups Are High-Risk Features!");
				process_object_menu();
				break;
				// switchable features
			default:
				if (lines[activeLineIndexWeapon].pState)
					*lines[activeLineIndexWeapon].pState = !(*lines[activeLineIndexWeapon].pState);
				if (lines[activeLineIndexWeapon].pUpdated)
					*lines[activeLineIndexWeapon].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexWeapon == 0)
						activeLineIndexWeapon = lineCount;
					activeLineIndexWeapon--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexWeapon++;
						if (activeLineIndexWeapon == lineCount)
							activeLineIndexWeapon = 0;
						waitTime = 150;
					}
	}
}

LPCSTR vehicleModels[63][10] = {
{"DINGHY", "DINGHY2", "DINGHY3", "DINGHY4", "JETMAX", "MARQUIS", "PREDATOR", "SEASHARK", "SEASHARK2", "SEASHARK3"},//boats
{ "SPEEDER", "SPEEDER2", "SQUALO", "SUBMERSIBLE", "SUBMERSIBLE2","SUNTRAP", "TORO", "TORO2", "TROPIC", "TROPIC2" },//boats
{ "TUG", "", "", "", "", "", "", "", "", "" },

{ "BENSON", "BIFF", "HAULER", "MULE", "MULE2","MULE3", "PACKER", "PHANTOM", "PHANTOM2", "POUNDER" },//Commercial
{ "STOCKADE","STOCKADE3", "", "", "", "", "", "", "", "" },//Commercial

{ "BLISTA", "BRIOSO", "DILETTANTE", "DILETTANTE2", "ISSI2","PANTO", "PRAIRIE", "RHAPSODY", "", "" }, //Compacts

{ "COGCABRIO", "EXEMPLAR", "F620", "FELON", "FELON2","JACKAL", "ORACLE", "ORACLE2", "SENTINEL", "SENTINEL2" }, //Coupes
{ "WINDSOR", "WINDSOR2", "ZION", "ZION2", "", "", "", "", "", "" },//Coupes

{ "BMX", "CRUISER", "FIXTER", "SCORCHER", "TRIBIKE","TRIBIKE2", "TRIBIKE3", "", "", "" }, //Cycles

{ "AMBULANCE", "FBI", "FBI2", "FIRETRUK", "LGUARD","PBUS", "PRANGER", "POLICE", "POLICE2", "POLICE3" },//Emergency
{ "POLICE4", "POLICEB", "POLICEOLD1", "POLICEOLD2", "POLICET","SHERIFF", "SHERIFF2", "RIOT", "", "" },// Emergency

{ "ANNIHILATOR", "BLIMP", "BLIMP2", "BUZZARD", "BUZZARD2","CARGOBOB", "CARGOBOB2", "CARGOBOB3", "CARGOBOB4", "FROGGER" },//Helicopters
{ "FROGGER2", "MAVERICK", "POLMAV", "SAVAGE", "SKYLIFT","SUPERVOLITO", "SUPERVOLITO2", "SWIFT", "SWIFT2", "VALKYRIE" },// Helicopters
{ "VALKYRIE2", "VOLATUS", "", "", "", "", "", "", "", "" },// Helicopters

{ "BULLDOZER", "CUTTER", "DUMP", "FLATBED", "GUARDIAN","HANDLER", "MIXER", "MIXER2", "RUBBLE", "TIPTRUCK" },//Industrial
{ "TIPTRUCK2", "", "", "", "", "", "", "", "", "" },//Industrial

{ "BARRACKS", "BARRACKS2", "BARRACKS3", "CRUSADER", "RHINO", "", "", "", "", "" },//Military

{ "AKUMA", "AVARUS", "BAGGER", "BATI", "BATI2","BF400", "CARBONRS", "CHIMERA", "CLIFFHANGER", "DAEMON" },//Motorcycles
{ "DAEMON2", "DEFILER", "DIABLOUS", "DIABLOUS2", "DOUBLE", "ENDURO", "ESSKEY","FAGGIO", "FAGGIO2", "FAGGIO3" },//Motorcycles
{ "FCR", "FCR2", "GARGOYLE", "HAKUCHOU", "HAKUCHOU2", "HEXER", "INNOVATION", "LECTRO", "MANCHEZ", "NEMESIS" },//Motorcycles
{"NIGHTBLADE", "PCJ", "RATBIKE", "RUFFIAN","SANCHEZ", "SANCHEZ2", "SANCTUS", "SHOTARO", "SOVEREIGN","THRUST" },//Motorcycles
{"VADER", "VINDICATOR", "VORTEX", "WOLFSBANE","ZOMBIEA", "ZOMBIEB", "", "", "", "" },//Motorcycles

{ "BLADE", "BUCCANEER", "BUCCANEER2", "CHINO", "CHINO2","COQUETTE3", "DOMINATOR", "DOMINATOR2", "DUKES", "DUKES2" },//Muscle
{ "GAUNTLET", "GAUNTLET2", "FACTION", "FACTION2", "FACTION3","HOTKNIFE", "LURCHER", "MOONBEAM", "MOONBEAM2", "NIGHTSHADE" },//Muscle
{ "PHOENIX", "PICADOR", "RATLOADER", "RATLOADER2", "RUINER", "RUINER2", "RUINER3","SABREGT", "SABREGT2", "SLAMVAN" },//Muscle
{ "SLAMVAN2", "SLAMVAN3","STALION", "STALION2", "TAMPA", "VIGERO", "VIRGO","VIRGO2", "VIRGO3", "VOODOO" },// Muscle
{"VOODOO2", "", "", "", "", "", "", "", "", ""},//Muscle

{ "BFINJECTION", "BIFTA", "BLAZER", "BLAZER2", "BLAZER3","BLAZER4", "BLAZER5", "BODHI2", "BRAWLER", "DLOADER" },//OffRoad
{ "DUBSTA3","DUNE", "DUNE2", "DUNE4", "DUNE5", "INSURGENT", "INSURGENT2", "KALAHARI","MARSHALL", "MESA3" },//OffRoad
{ "MONSTER", "RANCHERXL", "RANCHERXL2","REBEL", "REBEL2", "SANDKING", "SANDKING2", "TECHNICAL", "TECHNICAL2","TROPHYTRUCK" },//OffRoad
{"TROPHYTRUCK2", "", "", "", "", "", "", "", "", ""},//OffRoad

{ "BESRA", "CARGOPLANE", "CUBAN800", "DODO", "DUSTER","HYDRA", "JET", "LAZER", "LUXOR", "LUXOR2"},//Planes
{"MAMMATUS", "MILJET", "NIMBUS", "SHAMAL", "STUNT","TITAN", "VELUM", "VELUM2", "VESTRA", ""},//Planes

{"ASEA", "ASEA2", "ASTEROPE", "COG55", "COG552","COGNOSCENTI", "COGNOSCENTI2", "EMPEROR", "EMPEROR2", "EMPEROR3"},//Sedans
{"FUGITIVE", "GLENDALE", "INGOT", "INTRUDER", "LIMO2","PREMIER", "PRIMO", "PRIMO2", "REGINA", "ROMERO"},// Sedans
{"SCHAFTER2", "SCHAFTER5", "SCHAFTER6", "STANIER", "STRATUM","STRETCH", "SUPERD", "SURGE", "TAILGATER", "WARRENER" },//Sedans
{"WASHINGTON", "", "", "", "", "", "", "", "", ""},//Sedans

{ "AIRBUS", "BRICKADE", "BUS", "COACH", "RALLYTRUCK","RENTALBUS", "TAXI", "TOURBUS", "TRASH", "TRASH2" },//Service
{"WASTELANDER", "", "", "", "", "", "", "", "", ""},//Service

{ "ALPHA", "BANSHEE", "BESTIAGTS", "BLISTA2", "BLISTA3","BUFFALO", "BUFFALO2", "BUFFALO3", "CARBONIZZARE", "COMET2" },//Sports
{"COMET3","COQUETTE", "ELEGY", "ELEGY2", "FELTZER2", "FUROREGT", "FUSILADE","FUTO", "JESTER", "JESTER2" },//Sports
{"KHAMELION", "KURUMA","KURUMA2", "LYNX", "MASSACRO", "MASSACRO2", "NINEF","NINEF2", "OMNIS", "PENUMBRA" },//Sports
{"RAPIDGT", "RAPIDGT2","RAPTOR", "SCHAFTER3", "SCHAFTER4", "SCHWARTZER", "SEVEN70", "SPECTER", "SPECTER2","SULTAN" },//Sports
{"SURANO", "TAMPA2", "TROPOS", "VERLIERER2", "", "", "", "", "", ""},

{ "BTYPE", "BTYPE2", "BTYPE3", "CASCO", "COQUETTE2","FELTZER3", "JB700", "MAMBA", "MANANA", "MONROE" },//SportsClassics
{ "PEYOTE", "PIGALLE", "STINGER", "STINGERGT", "TORNADO","TORNADO2", "TORNADO3", "TORNADO4", "TORNADO5", "TORNADO6" },//SportsClassics
{"ZTYPE", "", "", "", "", "", "", "", "", ""},//SportsClassics

{ "ADDER", "BANSHEE2", "BULLET", "CHEETAH", "ENTITYXF","FMJ", "SHEAVA", "INFERNUS", "ITALIGTB", "ITALIGTB2" },//Super
{"NERO", "NERO2","OSIRIS", "LE7B", "PENETRATOR", "PFISTER811", "PROTOTIPO", "REAPER", "SULTANRS", "T20"},//Super
{"TEMPESTA", "TURISMOR", "TYRUS", "VACCA", "VOLTIC", "VOLTIC2", "ZENTORNO", "", "", ""},//Super

{ "BALLER", "BALLER2", "BALLER3", "BALLER4", "BALLER5","BALLER6", "BJXL", "CAVALCADE", "CAVALCADE2", "CONTENDER" },//SUVs
{ "DUBSTA", "DUBSTA2", "FQ2", "GRANGER", "GRESLEY","HABANERO", "HUNTLEY", "LANDSTALKER", "MESA", "MESA2" },//SUVs
{ "PATRIOT", "RADI", "ROCOTO", "SEMINOLE", "SERRANO","XLS", "XLS2", "", "", "" },//SUVs

{ "ARMYTANKER", "ARMYTRAILER", "ARMYTRAILER2", "BALETRAILER", "BOATTRAILER","DOCKTRAILER", "FREIGHTTRAILER", "GRAINTRAILER", "PROPTRAILER", "RAKETRAILER" },//Trailer
{ "TANKER", "TANKER2", "TR2", "TR3", "TR4","TRAILERLOGS", "TRAILERS", "TRAILERS2", "TRAILERS3", "TRAILERSMALL" },//Trailer
{"TRFLAT", "TVTRAILER", "", "", "", "", "", "", "", ""},//Trailer

{"CABLECAR", "FREIGHT", "FREIGHTCAR", "FREIGHTCONT1", "FREIGHTCONT2","FREIGHTGRAIN", "METROTRAIN", "TANKERCAR", "", ""},//Trains

{ "AIRTUG", "CADDY", "CADDY2", "DOCKTUG", "FORKLIFT","MOWER", "RIPLEY", "SADLER", "SADLER2", "SCRAP" },//Utility
{"TOWTRUCK", "TOWTRUCK2", "TRACTOR", "TRACTOR2", "TRACTOR3","UTILLITRUCK", "UTILLITRUCK2", "UTILLITRUCK3", "", ""},//Utility

{"BISON", "BISON2", "BISON3", "BOBCATXL", "BOXVILLE","BOXVILLE2", "BOXVILLE3", "BOXVILLE4", "BOXVILLE5", "BURRITO"},//Vans
{"BURRITO2","BURRITO3", "BURRITO4", "BURRITO5", "CAMPER", "GBURRITO","GBURRITO2", "JOURNEY", "MINIVAN", "MINIVAN2"},//Vans 
{"PARADISE","PONY", "PONY2", "RUMPO", "RUMPO2", "RUMPO3","SPEEDO", "SPEEDO2", "SURFER", "SURFER2"},
{"TACO","YOUGA", "YOUGA2", "", "", "", "", "", "", ""},//Vans

};

const char* veh_cat(int index)
{
	switch (index)
	{
	case 1: case 2: case 3: return "Boats";
	case 4: case 5: return "Commercial";
	case 6: return "Compacts";
	case 7: case 8: return "Coupes";
	case 9: return "Cycles";
	case 10: case 11: return "Emergency";
	case 12: case 13: case 14: return "Helicopters;";
	case 15: case 16: return "Industrial";
	case 17: return "Military";
	case 18: case 19: case 20: case 21: case 22: return "Motorcycles";
	case 23: case 24: case 25: case 26: case 27: return "Muscle";
	case 28: case 29: case 30: case 31: return "OffRoad";
	case 32: case 33: return "Planes";
	case 34: case 35: case 36: case 37: return "Sedans";
	case 38: case 39: return "Service";
	case 40: case 41: case  42: case 43: case 44: return "Sports";
	case 45: case 46: case  47:	return "SportsClassics";
	case 48: case 49: case 50: return "Super";
	case 51: case 52: case 53: return "Suv";
	case 54: case 55: case 56: return "Trailer";
	case 57: return "Trains";
	case 58: case 59: return "Utility";
	case 60: case 61: case 62: case 63:	return "Vans";
	default: return "Vehicles";
	}
}

int carspawnActiveLineIndex = 0;
int carspawnActiveItemIndex = 0;

static Vehicle do_spawn_vehicle(uint model)
{
	auto heading = ENTITY::GET_ENTITY_HEADING(PLAYER::PLAYER_PED_ID());
	auto coords = ENTITY::GET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), 0);

	float forward = 5.f;
	float xVect = forward * sin(degToRad(heading)) * -1.0f;
	float yVect = forward * cos(degToRad(heading));
	BOOL isAircraft = VEHICLE::IS_THIS_MODEL_A_HELI(model) || VEHICLE::IS_THIS_MODEL_A_PLANE(model);

	Vehicle vehicle = NULL;
	if (isAircraft && featureVehWrapInSpawned)
	{
		vehicle = VEHICLE::CREATE_VEHICLE(model, coords.x + xVect, coords.y + yVect, coords.z + 1000, heading, TRUE, TRUE);
		VEHICLE::SET_VEHICLE_FORWARD_SPEED(vehicle, 500.0f);
		VEHICLE::SET_HELI_BLADES_FULL_SPEED(vehicle);
	}
	else
	{
		vehicle = VEHICLE::CREATE_VEHICLE(model, coords.x + xVect, coords.y + yVect, coords.z, heading, TRUE, TRUE);
		VEHICLE::SET_VEHICLE_ON_GROUND_PROPERLY(vehicle);
	}

	DECORATOR::DECOR_SET_INT(vehicle, "MPBitset", 0);

	if (featureVehWrapInSpawned) PED::SET_PED_INTO_VEHICLE(PLAYER::PLAYER_PED_ID(), vehicle, -1);

	return vehicle;
}


bool process_carspawn_menu()
{
	DWORD waitTime = 150;
	const int lineCount = 63;
	const int itemCount = 10;
	const int itemCountLastLine = 1;


	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			char caption[32];
			sprintf_s(caption, "%s\tALL (%d / %d)", veh_cat(carspawnActiveLineIndex + 1), carspawnActiveLineIndex + 1, lineCount);
			draw_menu_line(caption, 350.0f, 15.0f, 18.0f, 0.0f, 5.0f, false, true);
			for (int i = 0; i < itemCount; i++)
				if (strlen(vehicleModels[carspawnActiveLineIndex][i]))
					draw_menu_line(vehicleModels[carspawnActiveLineIndex][i], 100.0f, 5.0f, 200.0f, 100.0f + i * 110.0f, 5.0f, i == carspawnActiveItemIndex, false, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		bool bSelect, bBack, bUp, bDown, bLeft, bRight;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, &bLeft, &bRight);

		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			LPCSTR modelName = vehicleModels[carspawnActiveLineIndex][carspawnActiveItemIndex];

			uint model = $(modelName);
			STREAMING::REQUEST_MODEL(model);
			DWORD now = GetTickCount();
			while (!STREAMING::HAS_MODEL_LOADED(model) && GetTickCount() < now + 5000)
			{
				WAIT(0);
			}

			if (!STREAMING::HAS_MODEL_LOADED(model))
			{
				std::ostringstream ss2;
				ss2 << "~HUD_COLOUR_RED~ Timed out requesting  " << modelName << " : 0x" << model;
				set_status_text(ss2.str());
				return true;
			}

			auto Vehicle = do_spawn_vehicle(model);
			if (Vehicle)
			{
				std::ostringstream ss;
				ss << "~HUD_COLOUR_GREEN~ Spawned " << modelName << " : 0x" << model;
				set_status_text(ss.str());
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);
				ENTITY::SET_VEHICLE_AS_NO_LONGER_NEEDED(&Vehicle);

				return true;
			}
		}
		else
			if (bBack)
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bRight)
				{
					menu_beep(NAV_LEFT_RIGHT);
					carspawnActiveItemIndex++;
					int itemsMax = (carspawnActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
					if (carspawnActiveItemIndex == itemsMax)
						carspawnActiveItemIndex = 0;
					waitTime = 100;
				}
				else
					if (bLeft)
					{
						menu_beep(NAV_LEFT_RIGHT);
						if (carspawnActiveItemIndex == 0)
							carspawnActiveItemIndex = (carspawnActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
						carspawnActiveItemIndex--;
						waitTime = 100;
					}
					else
						if (bUp)
						{
							menu_beep(NAV_UP_DOWN);
							if (carspawnActiveLineIndex == 0)
								carspawnActiveLineIndex = lineCount;
							carspawnActiveLineIndex--;
							waitTime = 200;
						}
						else
							if (bDown)
							{
								menu_beep(NAV_UP_DOWN);
								carspawnActiveLineIndex++;
								if (carspawnActiveLineIndex == lineCount)
									carspawnActiveLineIndex = 0;
								waitTime = 200;
							}
		if (carspawnActiveLineIndex == (lineCount - 1))
			if (carspawnActiveItemIndex >= itemCountLastLine)
				carspawnActiveItemIndex = 0;
	}
	return false;
}

int activeLineIndexPaint = 0;

void process_paint_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 33;

	std::string caption = "Respray Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Enhanced Random ~HUD_COLOUR_GREEN~:",	NULL,NULL },
		{ "Random Primary RGB ~HUD_COLOUR_GREEN~:",	NULL,NULL },
		{ "Random Secondary RGB ~HUD_COLOUR_GREEN~:",	NULL,NULL },
		{ "Black ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Silver ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Formula Red ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Candy Red ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Gold ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Orange ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Olive Green ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Bright Green ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Matte Lime Green ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Metallic Gas Green ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Midnight City Green ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Galaxy Blue ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Diamond Blue ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Racing Blue ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Raceing Yellow ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Saddle Brown ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Cream ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Ice White ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Frost White ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Chrome ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Hot Pink ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Pfister Pink ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Midnight Purple ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Pure Gold ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Secret Gold ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Bronze ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Graphite ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Black STEEL ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Dark STEEL ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Cast Iron Silver ~HUD_COLOUR_GREEN~:",NULL,NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexPaint)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 2.0f, 60.0f + i * 20.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexPaint].text, lines[activeLineIndexPaint].pState),
				lineWidth + 0.0f, 2.0f, 60.0f + activeLineIndexPaint * 20.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			Player Player = PLAYER::PLAYER_ID();
			Ped PlayerPed = PLAYER::PLAYER_PED_ID();
			BOOL PlayerExists = ENTITY::DOES_ENTITY_EXIST(PlayerPed);
			Vehicle VehicleHandle = PED::GET_VEHICLE_PED_IS_USING(PlayerPed);

			switch (activeLineIndexPaint)
			{
			case 0:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, rand() % 157, rand() % 157);
				break;
			case 1:
				VEHICLE::SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(VehicleHandle, rand() % 255, rand() % 255, rand() % 255);
				break;
			case 2:
				VEHICLE::SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(VehicleHandle, rand() % 255, rand() % 255, rand() % 255);
				break;
			case 3:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 0, 0);//black
				break;
			case 4:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 4, 4);//Silver
				break;
			case 5:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 29, 29);//Formula Red
				break;
			case 6:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 35, 35);//Candy Red
				break;
			case 7:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 37, 37);//Gold
				break;
			case 8:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 38, 38);//Orange
				break;
			case 9:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 52, 52);//Olive Green
				break;
			case 10:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 53, 53);//Bright Green
				break;
			case 11:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 55, 55);//Matte Lime Green
				break;
			case 12:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 54, 54);//Metallic Gasoline Green
				break;
			case 13:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 59, 59);//Metallic Midnight Green
				break;
			case 14:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 61, 61);//Galaxy Blue
				break;
			case 15:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 67, 67);//Diamond Blue
				break;
			case 16:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 73, 73);//Racing Blue
				break;
			case 17:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 89, 89);//Race Yellow
				break;
			case 18:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 98, 98);//Saddle Brown
				break;
			case 19:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 107, 107);//Cream
				break;
			case 20:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 111, 111);//Ice White
				break;
			case 21:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 112, 112);//Frost White
				break;
			case 22:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 120, 120);//Chrome
				break;
			case 23:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 135, 135);//Hot Pink
				break;
			case 24:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 137, 137);//Pfister Pink
				break;
			case 25:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 142, 142);//Midnight Purple
				break;
			case 26:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 158, 158);//Pure Gold
				break;
			case 27:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 160, 160);//Secret Gold
				break;
			case 28:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 90, 90);//Bronze
				break;
			case 29:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 1, 1);//Graphite
				break;
			case 30:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 2, 2);//Black Steel
				break;
			case 31:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 3, 3);//Dark Steel
				break;
			case 32:
				VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 10, 10);//Cast Iron Silver
				break;
				// switchable features
			default:
				if (lines[activeLineIndexPaint].pState)
					*lines[activeLineIndexPaint].pState = !(*lines[activeLineIndexPaint].pState);
				if (lines[activeLineIndexPaint].pUpdated)
					*lines[activeLineIndexPaint].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexPaint == 0)
						activeLineIndexPaint = lineCount;
					activeLineIndexPaint--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexPaint++;
						if (activeLineIndexPaint == lineCount)
							activeLineIndexPaint = 0;
						waitTime = 150;

					}
	}
}

int activeLineIndexCarMenu = 0;

void process_car_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 6;

	std::string caption = "Modifications";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Respray ~HUD_COLOUR_BLUE~>",NULL,NULL },
		{ "Neon LightS ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "License Plates ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Window Tint ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Auto Customization ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Bullet Proof Tires ~HUD_COLOUR_GREEN~:",&featureVehInvincibleWheels,&featureVehInvincibleWheelsUpdated }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexCarMenu)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexCarMenu].text, lines[activeLineIndexCarMenu].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexCarMenu * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			Player Player = PLAYER::PLAYER_ID();
			Ped PlayerPed = PLAYER::PLAYER_PED_ID();
			BOOL PlayerExists = ENTITY::DOES_ENTITY_EXIST(PlayerPed);
			Vehicle VehicleHandle = PED::GET_VEHICLE_PED_IS_USING(PlayerPed);

			switch (activeLineIndexCarMenu)
			{
			case 0:
				notifyAboveMap("~HUD_COLOUR_RED~: ~HUD_COLOUR_WHITE~Paint Selection");
				NotifyBottomCentre("~HUD_COLOUR_RED~: ~HUD_COLOUR_WHITE~Paint Selection");
				process_paint_menu();
				break;
			case 1:
				VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 0, 1);
				VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 1, 1);
				VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 2, 1);
				VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 3, 1);
				VEHICLE::_SET_VEHICLE_NEON_LIGHTS_COLOUR(VehicleHandle, rand() % 255, rand() % 255, rand() % 255);
				break;
			case 2:
				VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(VehicleHandle, rand() % 5);
				VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleHandle, " ");
				break;
			case 3:
				VEHICLE::SET_VEHICLE_WINDOW_TINT(VehicleHandle, rand() % 3);
				break;
			case 4:
				if (ENTITY::DOES_ENTITY_EXIST(PlayerPed) && PED::IS_PED_IN_ANY_VEHICLE(PlayerPed, 0))
				{
					VEHICLE::SET_VEHICLE_FIXED(PED::GET_VEHICLE_PED_IS_USING(PlayerPed));
					VEHICLE::SET_VEHICLE_DEFORMATION_FIXED(PED::GET_VEHICLE_PED_IS_USING(PlayerPed));
					VEHICLE::SET_VEHICLE_DIRT_LEVEL(PED::GET_VEHICLE_PED_IS_USING(PlayerPed), 0);
					VEHICLE::SET_VEHICLE_TYRES_CAN_BURST(VehicleHandle, 0);
					VEHICLE::SET_VEHICLE_WHEELS_CAN_BREAK(VehicleHandle, 0);
					VEHICLE::SET_VEHICLE_HAS_STRONG_AXLES(VehicleHandle, 1);
					VEHICLE::SET_VEHICLE_MOD_KIT(VehicleHandle, 0);
					VEHICLE::SET_VEHICLE_COLOURS(VehicleHandle, 135, 120);
					VEHICLE::TOGGLE_VEHICLE_MOD(VehicleHandle, 18, 1);
					VEHICLE::TOGGLE_VEHICLE_MOD(VehicleHandle, 22, 1);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 16, 5, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 12, 2, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 11, 3, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 14, 14, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 15, 3, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 13, 2, 0);
					VEHICLE::SET_VEHICLE_WHEEL_TYPE(VehicleHandle, rand() % 6);
					VEHICLE::SET_VEHICLE_WINDOW_TINT(VehicleHandle, 1);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 23, 14, 2);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 0, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 1, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 2, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 3, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 4, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 5, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 6, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 7, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 8, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 9, 1, 0);
					VEHICLE::SET_VEHICLE_MOD(VehicleHandle, 10, 1, 0);
					VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 0, 1);
					VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 1, 1);
					VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 2, 1);
					VEHICLE::_SET_VEHICLE_NEON_LIGHT_ENABLED(PED::GET_VEHICLE_PED_IS_IN(PlayerPed, 0), 3, 1);
					VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(VehicleHandle, 6);
					VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleHandle, "SudoMod");
				}
				break;
				// switchable features
			default:
				if (lines[activeLineIndexCarMenu].pState)
					*lines[activeLineIndexCarMenu].pState = !(*lines[activeLineIndexCarMenu].pState);
				if (lines[activeLineIndexCarMenu].pUpdated)
					*lines[activeLineIndexCarMenu].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexCarMenu == 0)
						activeLineIndexCarMenu = lineCount;
					activeLineIndexCarMenu--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexCarMenu++;
						if (activeLineIndexCarMenu == lineCount)
							activeLineIndexCarMenu = 0;
						waitTime = 150;

					}
	}
}

int activeLineIndexVeh = 0;

void process_veh_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 11;

	std::string caption = "Vehicle  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Modifications ~HUD_COLOUR_BLUE~>",NULL,NULL },
		{ "Repair Damage ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Flip Upright ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Bypass Sell Timer ~HUD_COLOUR_GREEN~:",NULL,NULL },
		{ "Spawn Vehicle ~HUD_COLOUR_BLUE~>",NULL,NULL },
		{ "Warp into Spawned ~HUD_COLOUR_GREEN~:",&featureVehWrapInSpawned, NULL },
		{ "Seat Belt ~HUD_COLOUR_GREEN~:",&featureVehSeatbelt, &featureVehSeatbeltUpdated },
		{ "Invincible ~HUD_COLOUR_GREEN~:",&featureVehInvincible, &featureVehInvincibleUpdated },
		{ "Speed Boost ~HUD_COLOUR_GREEN~:",&featureVehSpeedBoost, NULL },
		{ "Maximum Traction ~HUD_COLOUR_GREEN~:", &featureVehStickyCar, NULL },
		{ "Minimum Traction ~HUD_COLOUR_GREEN~:", &featureVehSlide, NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexVeh)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexVeh].text, lines[activeLineIndexVeh].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexVeh * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);

			// common variables
			Player Player = PLAYER::PLAYER_ID();
			Ped PlayerPed = PLAYER::PLAYER_PED_ID();
			BOOL PlayerExists = ENTITY::DOES_ENTITY_EXIST(PlayerPed);
			Vehicle VehicleHandle = PED::GET_VEHICLE_PED_IS_USING(PlayerPed);

			switch (activeLineIndexVeh)
			{
			case 0:
				notifyAboveMap("~HUD_COLOUR_RED~Beta: ~HUD_COLOUR_WHITE~Modification Selector");
				NotifyBottomCentre("~HUD_COLOUR_RED~Beta: ~HUD_COLOUR_WHITE~Modification Selector");
				process_car_menu();
				break;
			case 1:
				if (PlayerExists) {
					if (PED::IS_PED_IN_ANY_VEHICLE(PlayerPed, 0))
						VEHICLE::SET_VEHICLE_FIXED(PED::GET_VEHICLE_PED_IS_USING(PlayerPed));
					VEHICLE::SET_VEHICLE_DEFORMATION_FIXED(PED::GET_VEHICLE_PED_IS_USING(PlayerPed));
					VEHICLE::SET_VEHICLE_DIRT_LEVEL(PED::GET_VEHICLE_PED_IS_USING(PlayerPed), 0);
				}
				break;
			case 2:
				VEHICLE::SET_VEHICLE_ON_GROUND_PROPERLY(VehicleHandle);
				break;
			case 3:
				STATS::STAT_SET_INT($("MPPLY_VEHICLE_SELL_TIME"), 0, 1);
				break;
			case 4:
				if (process_carspawn_menu()) return;
				break;
				// switchable features
			default:
				if (lines[activeLineIndexVeh].pState)
					*lines[activeLineIndexVeh].pState = !(*lines[activeLineIndexVeh].pState);
				if (lines[activeLineIndexVeh].pUpdated)
					*lines[activeLineIndexVeh].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexVeh == 0)
						activeLineIndexVeh = lineCount;
					activeLineIndexVeh--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexVeh++;
						if (activeLineIndexVeh == lineCount)
							activeLineIndexVeh = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexHUD = 0;

void process_hudfx_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 15;

	std::string caption = "Hud Effects";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Race Turbo ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Rampage ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Death Fail Out ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Explosion Josh ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Sniper Overlay ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Drugs Driving ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Chop Vision ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Switch In ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Switch Out ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Focus In ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Focus Out ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Race Crash ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Taze Me ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Flight ~HUD_COLOUR_GREEN~:",NULL,	NULL },
		{ "Stop All Effects ~HUD_COLOUR_GREEN~:",NULL,	NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexHUD)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexHUD].text, lines[activeLineIndexHUD].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexHUD * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			switch (activeLineIndexHUD)
			{
			case 0:
				//code
				featureHudFx = !featureHudFx;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("RaceTurbo", 999999999, false);
				break;
			case 1:
				//code
				featureHudFx = !featureHudFx1;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("Rampage", 999999999, false);
				break;
			case 2:
				//code
				featureHudFx = !featureHudFx2;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("DeathFailOut", 999999999, false);
				break;
			case 3:
				//code
				featureHudFx = !featureHudFx3;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("ExplosionJosh3", 999999999, false);
				break;
			case 4:
				//code
				featureHudFx = !featureHudFx4;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("SniperOverlay", 0, false);
				break;
			case 5:
				//code
				featureHudFx = !featureHudFx5;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("DrugsDrivingIn", 0, false);
				break;
			case 6:
				//code
				featureHudFx = !featureHudFx6;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("ChopVision", 999999999, false);
				break;
			case 7:
				//code
				featureHudFx = !featureHudFx7;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("SwitchHUDIn", 999999999, false);
				break;
			case 8:
				//code
				featureHudFx = !featureHudFx8;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("SwitchHUDOut", 999999999, false);
				break;
			case 9:
				//code
				featureHudFx = !featureHudFx9;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("FocusIn", 999999999, false);
				break;
			case 10:
				//code
				featureHudFx = !featureHudFx10;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("FocusOut", 999999999, false);
				break;
			case 11:
				//code
				featureHudFx = !featureHudFx11;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("MP_race_crash", 999999999, false);
				break;
			case 12:
				//code
				featureHudFx = !featureHudFx12;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("Dont_tazeme_bro", 999999999, false);
				break;
			case 13:
				//code
				featureHudFx = !featureHudFx13;
				if (featureHudFx)
					GRAPHICS::_START_SCREEN_EFFECT("DMT_flight", 999999999, false);
				break;
			case 14:
				GRAPHICS::_STOP_SCREEN_EFFECT("RaceTurbo");
				GRAPHICS::_STOP_SCREEN_EFFECT("Rampage");
				GRAPHICS::_STOP_SCREEN_EFFECT("DeathFailOut");
				GRAPHICS::_STOP_SCREEN_EFFECT("ExplosionJosh3");
				GRAPHICS::_STOP_SCREEN_EFFECT("SniperOverlay");
				GRAPHICS::_STOP_SCREEN_EFFECT("DrugsDrivingIn");
				GRAPHICS::_STOP_SCREEN_EFFECT("ChopVision");
				GRAPHICS::_STOP_SCREEN_EFFECT("SwitchHUDIn");
				GRAPHICS::_STOP_SCREEN_EFFECT("SwitchHUDOut");
				GRAPHICS::_STOP_SCREEN_EFFECT("FocusIn");
				GRAPHICS::_STOP_SCREEN_EFFECT("FocusOut");
				GRAPHICS::_STOP_SCREEN_EFFECT("MP_race_crash");
				GRAPHICS::_STOP_SCREEN_EFFECT("Dont_tazeme_bro");
				GRAPHICS::_STOP_SCREEN_EFFECT("DMT_flight");
				break;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexHUD == 0)
						activeLineIndexHUD = lineCount;
					activeLineIndexHUD--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexHUD++;
						if (activeLineIndexHUD == lineCount)
							activeLineIndexHUD = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexWorld = 0;

void process_world_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 7;

	std::string caption = "World  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Moon Gravity ~HUD_COLOUR_GREEN~:",	&featureWorldMoonGravity,	NULL },
		{ "Random Cops ~HUD_COLOUR_GREEN~:",		&featureWorldRandomCops,	NULL },
		{ "Random Trains ~HUD_COLOUR_GREEN~:",	&featureWorldRandomTrains,	NULL },
		{ "Random Boats ~HUD_COLOUR_GREEN~:",	&featureWorldRandomBoats,	NULL },
		{ "Random Garbage Trucks ~HUD_COLOUR_GREEN~:",	&featureWorldGarbageTrucks,	NULL },
		{ "Cut Electricity  ~HUD_COLOUR_GREEN~:",	&featureWorldBlackOut,	NULL },
		{ "Screen Effects  ~HUD_COLOUR_BLUE~>",	NULL,	NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexWorld)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexWorld].text, lines[activeLineIndexWorld].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexWorld * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			switch (activeLineIndexWorld)
			{
			case 0:
				featureWorldMoonGravity = !featureWorldMoonGravity;
				GAMEPLAY::SET_GRAVITY_LEVEL(featureWorldMoonGravity ? 2 : 0);
				break;
			case 1:
				// featureWorldRandomCops being set in update_features
				PED::SET_CREATE_RANDOM_COPS(!featureWorldRandomCops);
				break;
			case 2:
				featureWorldRandomTrains = !featureWorldRandomTrains;
				VEHICLE::SET_RANDOM_TRAINS(featureWorldRandomTrains);
				break;
			case 3:
				featureWorldRandomBoats = !featureWorldRandomBoats;
				VEHICLE::SET_RANDOM_BOATS(featureWorldRandomBoats);
				break;
			case 4:
				featureWorldGarbageTrucks = !featureWorldGarbageTrucks;
				VEHICLE::SET_GARBAGE_TRUCKS(featureWorldGarbageTrucks);
				break;
			case 5:
				// blackout
				featureWorldBlackOut = !featureWorldBlackOut;
				GRAPHICS::_SET_BLACKOUT(featureWorldBlackOut);
				break;
			case 6:
				process_hudfx_menu();
				break;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexWorld == 0)
						activeLineIndexWorld = lineCount;
					activeLineIndexWorld--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexWorld++;
						if (activeLineIndexWorld == lineCount)
							activeLineIndexWorld = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexTime = 0;

void process_time_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 4;

	std::string caption = "Time  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "1 Hour Forward ~HUD_COLOUR_GREEN~:",	 NULL,				 NULL },
		{ "1 Hour Backward ~HUD_COLOUR_GREEN~:",	 NULL,				 NULL },
		{ "Freeze Time ~HUD_COLOUR_GREEN~:",	 &featureTimePaused, &featureTimePausedUpdated },
		{ "Sync System Time ~HUD_COLOUR_GREEN~:", &featureTimeSynced, NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexTime)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexTime].text, lines[activeLineIndexTime].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexTime * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			switch (activeLineIndexTime)
			{
				// hour forward/backward
			case 0:
			case 1:
			{
				int h = TIME::GET_CLOCK_HOURS();
				if (activeLineIndexTime == 0) h = (h == 23) ? 0 : h + 1; else h = (h == 0) ? 23 : h - 1;
				int m = TIME::GET_CLOCK_MINUTES();
				TIME::SET_CLOCK_TIME(h, m, 0);
				char text[32];
				sprintf_s(text, "~HUD_COLOUR_ORANGE~TIME %d:%d", h, m);
				set_status_text(text);
			}
			break;
			// switchable features
			default:
				if (lines[activeLineIndexTime].pState)
					*lines[activeLineIndexTime].pState = !(*lines[activeLineIndexTime].pState);
				if (lines[activeLineIndexTime].pUpdated)
					*lines[activeLineIndexTime].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexTime == 0)
						activeLineIndexTime = lineCount;
					activeLineIndexTime--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexTime++;
						if (activeLineIndexTime == lineCount)
							activeLineIndexTime = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexWeather = 0;

void process_weather_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 16;

	std::string caption = "Weather  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Windy ~HUD_COLOUR_GREEN~:",	 	 &featureWeatherWind,	NULL },
		{ "Persistive ~HUD_COLOUR_GREEN~:",  &featureWeatherPers,	NULL },
		{ "Extra Sunny",	 NULL,					NULL },
		{ "Clear",		 NULL,					NULL },
		{ "Cloudy",		 NULL,					NULL },
		{ "Smoggy",		 NULL,					NULL },
		{ "Foggy",	 	 NULL,					NULL },
		{ "Overcast",	 NULL,					NULL },
		{ "Rainning",		 NULL,					NULL },
		{ "Thunder",		 NULL,					NULL },
		{ "Clearing",	 NULL,					NULL },
		{ "Neutral",		 NULL,					NULL },
		{ "Snow",		 NULL,					NULL },
		{ "Blizzard",	 NULL,					NULL },
		{ "Snowlight",	 NULL,					NULL },
		{ "XMAS",		 NULL,					NULL }
	};


	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexWeather)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexWeather].text, lines[activeLineIndexWeather].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexWeather * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			switch (activeLineIndexWeather)
			{
				// wind
			case 0:
				featureWeatherWind = !featureWeatherWind;
				if (featureWeatherWind)
				{
					GAMEPLAY::SET_WIND(1.0f);
					GAMEPLAY::SET_WIND_SPEED(11.99f);
					GAMEPLAY::SET_WIND_DIRECTION(ENTITY::GET_ENTITY_HEADING(PLAYER::PLAYER_PED_ID()));
				}
				else
				{
					GAMEPLAY::SET_WIND(0.0);
					GAMEPLAY::SET_WIND_SPEED(0.0);
				}
				break;
				// set persist
			case 1:
				featureWeatherPers = !featureWeatherPers;
				break;
				// set weather
			default:
				GAMEPLAY::CLEAR_OVERRIDE_WEATHER();
				if (featureWeatherPers)
				{
					GAMEPLAY::SET_OVERRIDE_WEATHER((char *)lines[activeLineIndexWeather].text);
				}
				else
				{
					GAMEPLAY::SET_WEATHER_TYPE_NOW_PERSIST((char *)lines[activeLineIndexWeather].text);
					GAMEPLAY::CLEAR_WEATHER_TYPE_PERSIST();
				}
				set_status_text(lines[activeLineIndexWeather].text);
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexWeather == 0)
						activeLineIndexWeather = lineCount;
					activeLineIndexWeather--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexWeather++;
						if (activeLineIndexWeather == lineCount)
							activeLineIndexWeather = 0;
						waitTime = 150;
					}
	}
}

int pedspawnActiveLineIndex = 0;
int pedspawnActiveItemIndex = 0;

bool process_pedspawn_menu()
{
	DWORD waitTime = 150;
	const int lineCount = 69;
	const int itemCount = 10;
	const int itemCountLastLine = itemCount;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			char caption[32];
			sprintf_s(caption, "Pedestrian  Selection  %d / %d", pedspawnActiveLineIndex + 1, lineCount);
			draw_menu_line(caption, 350.0, 15.0, 18.0, 0.0, 5.0, false, true);
			for (int i = 0; i < itemCount; i++)
				if (strlen(pedModels[pedspawnActiveLineIndex][i]) || strcmp(pedModelNames[pedspawnActiveLineIndex][i], "None") == 0)
					draw_menu_line(pedModelNames[pedspawnActiveLineIndex][i], 100.0f, 5.0f, 200.0f, 100.0f + i * 110.0f, 5.0f, i == pedspawnActiveItemIndex, false, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		bool bSelect, bBack, bUp, bDown, bLeft, bRight;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, &bLeft, &bRight);

		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			Player player = PLAYER::PLAYER_ID();
			Ped playerPed = PLAYER::PLAYER_PED_ID();
			BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
			Vector3 coords = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(playerPed, 0.0f, 3.5f, 0.0f);
			DWORD model = $((char *)pedModels[pedspawnActiveLineIndex][pedspawnActiveItemIndex]);
			if (STREAMING::IS_MODEL_IN_CDIMAGE(model) && STREAMING::IS_MODEL_VALID(model))
			{
				STREAMING::REQUEST_MODEL(model);
				while (!STREAMING::HAS_MODEL_LOADED(model))	WAIT(0);
				Ped Bank1 = PED::CREATE_PED(690, model, coords.x, coords.y, coords.z, ENTITY::GET_ENTITY_HEADING(playerPed), TRUE, TRUE);
				PED::SET_PED_MONEY(Bank1, rand() % 10000);
				WAIT(0);

				pedspawn_used = true;
				WAIT(100);
				STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);
				waitTime = 200;
			}
		}
		else
			if (bBack)
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bRight)
				{
					menu_beep(NAV_LEFT_RIGHT);
					pedspawnActiveItemIndex++;
					int itemsMax = (pedspawnActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
					if (pedspawnActiveItemIndex == itemsMax)
						pedspawnActiveItemIndex = 0;
					waitTime = 100;
				}
				else
					if (bLeft)
					{
						menu_beep(NAV_LEFT_RIGHT);
						if (pedspawnActiveItemIndex == 0)
							pedspawnActiveItemIndex = (pedspawnActiveLineIndex == (lineCount - 1)) ? itemCountLastLine : itemCount;
						pedspawnActiveItemIndex--;
						waitTime = 100;
					}
					else
						if (bUp)
						{
							menu_beep(NAV_UP_DOWN);
							if (pedspawnActiveLineIndex == 0)
								pedspawnActiveLineIndex = lineCount;
							pedspawnActiveLineIndex--;
							waitTime = 200;
						}
						else
							if (bDown)
							{
								menu_beep(NAV_UP_DOWN);
								pedspawnActiveLineIndex++;
								if (pedspawnActiveLineIndex == lineCount)
									pedspawnActiveLineIndex = 0;
								waitTime = 200;
							}
		if (pedspawnActiveLineIndex == (lineCount - 1))
			if (pedspawnActiveItemIndex >= itemCountLastLine)
				pedspawnActiveItemIndex = 0;
	}
	return false;
}

void kill_pedestrians()
{
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();
	BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
	Ped pedestrian;
	Vector3 position = ENTITY::GET_ENTITY_COORDS(playerPed, 1);
	if (PED::GET_CLOSEST_PED(position.x, position.y, position.z, 999999999.f, 1, 0, &pedestrian, 1, 1, -1))
		ENTITY::SET_ENTITY_HEALTH(pedestrian, 0);
}

void clear_pedestrians() {
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();
	BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
	Vector3 position = ENTITY::GET_ENTITY_COORDS(playerPed, 1);
	GAMEPLAY::CLEAR_AREA_OF_PEDS(position.x, position.y, position.z, 999999999.f, -1);
}

void clear_cop_pedestrians() {
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();
	BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
	Vector3 position = ENTITY::GET_ENTITY_COORDS(playerPed, 1);
	GAMEPLAY::CLEAR_AREA_OF_COPS(position.x, position.y, position.z, 999999999.f, -1);
}

void clear_pedestrians_vehicles()
{
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();
	Vector3 position = ENTITY::GET_ENTITY_COORDS(playerPed, 1);
	GAMEPLAY::CLEAR_AREA_OF_VEHICLES(position.x, position.y, position.z, 999999999.f, 0, 0, 0, 0, 0);
}

int activeLineIndexMisc = 0;
void process_misc_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 8;

	std::string caption = "Miscellaneous  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "MObile Radio ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Next Radio Station ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Pedestrian Selection ~HUD_COLOUR_BLUE~>",	NULL,					NULL },
		{ "Kill Pedestrians ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Clear Pedestrians ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Clear Cop Pedestrians ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Clear Area Vehicles ~HUD_COLOUR_GREEN~:",	NULL,					NULL },
		{ "Always Hide HUD ~HUD_COLOUR_GREEN~:",			&featureMiscHideHud,	NULL }
	};


	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexMisc)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexMisc].text, lines[activeLineIndexMisc].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexMisc * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			Ped playerPed = PLAYER::PLAYER_PED_ID();
			BOOL PlayerExists = ENTITY::DOES_ENTITY_EXIST(playerPed);
			menu_beep(NAV_SELECT);
			switch (activeLineIndexMisc)
			{
			case 0:
				//featureMiscMobileRadio=!featureMiscMobileRadio;
				if (featureMiscMobileRadio) {
					if (PlayerExists) {
						AUDIO::SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(true);
					}
					else {
						if (PlayerExists) {
							AUDIO::SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(false);
						}
					}
				}
				break;
				// next radio track
			case 1:

				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()) &&
					PED::IS_PED_IN_ANY_VEHICLE(PLAYER::PLAYER_PED_ID(), 0))
					AUDIO::SKIP_RADIO_FORWARD();
				break;
			case 2:
				if (process_pedspawn_menu())	return;
				break;
			case 3:
				kill_pedestrians();
				break;
			case 4:
				clear_pedestrians();
				break;
			case 5:
				clear_cop_pedestrians();
				break;
			case 6:
				clear_pedestrians_vehicles();
				break;
				// switchable features
			default:
				if (lines[activeLineIndexMisc].pState)
					*lines[activeLineIndexMisc].pState = !(*lines[activeLineIndexMisc].pState);
				if (lines[activeLineIndexMisc].pUpdated)
					*lines[activeLineIndexMisc].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexMisc == 0)
						activeLineIndexMisc = lineCount;
					activeLineIndexMisc--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexMisc++;
						if (activeLineIndexMisc == lineCount)
							activeLineIndexMisc = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexRec = 0;

void process_rec_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 20;

	std::string caption = "Recovery  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Unlock Purchases ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Max Stats ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Clothes ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock LSC ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "UNlock Tattoos ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Max Armor & SNacks ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Heist Vehicles ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Weapon Skins ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Max Playtime Edited ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Chrome Rims ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Exclusive Shirts ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Redesign Character 1 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Redesign Character 2 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Skip Tutorials ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Trophies ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Unlock Hairatyles ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Purchase Weapons ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Roosevelt Listed ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Clear Badsport ~HUD_COLOUR_GREEN~:",	NULL,NULL },
		{ "Clear Cheat Reports ~HUD_COLOUR_GREEN~:",	NULL,NULL },
	};


	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexRec)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexRec].text, lines[activeLineIndexRec].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexRec * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			switch (activeLineIndexRec)
			{
			case 0:
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_1_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_2_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_3_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_4_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_5_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_6_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_7_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_8_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_9_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_10_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_11_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_12_FM_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE3"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE4"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE5"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE6"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE7"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE8"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE9"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE10"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE11"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHASE12"), -1, 1);
				break;
			case 1:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_STAM"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_STRN"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_LUNG"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_DRIV"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_FLY"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_SHO"), 100, true);
					STATS::STAT_SET_INT($("MP0_SCRIPT_INCREASE_STL"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_STAM"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_STRN"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_LUNG"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_DRIV"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_FLY"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_SHO"), 100, true);
					STATS::STAT_SET_INT($("MP1_SCRIPT_INCREASE_STL"), 100, true);
				}
				break;
			case 2:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_JBIB_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_LEGS_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_FEET_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_BERD_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_8"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_9"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_PROPS_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_OUTFIT"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_5"), -1, 1);;
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_HAIR_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_JBIB_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_6"), -1, 1);;
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_LEGS_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_FEET_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_BERD_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_8"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_9"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_PROPS_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_OUTFIT"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_TORSO"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_SPECIAL2_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_DECL"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_TEETH"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_TEETH_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_TEETH_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_TORSO"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_SPECIAL2_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_DECL"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_TEETH"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_TEETH_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_CLTHS_ACQUIRED_TEETH_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_0"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_8"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_9"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_11"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_12"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_13"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_14"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_15"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_16"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_17"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_18"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_19"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_21"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_22"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_23"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_24"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_24"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_25"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_26"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_27"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_28"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_29"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_30"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_31"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_32"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_33"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_34"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_35"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_36"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_37"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_38"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_39"), -1, 1);
					STATS::STAT_SET_INT($("MP0_DLC_APPAREL_ACQUIRED_40"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHESGV_BS_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_8"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_9"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_11"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_12"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_13"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_11"), -1, 1);
					STATS::STAT_SET_INT($("MP0_ADMIN_CLOTHES_GV_BS_12"), -1, 1);
				}
				break;
			case 3:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_BOOL($("MP0_AWD_FMRACEWORLDRECHOLDER"), 1, 0);
					STATS::STAT_SET_INT($("MP0_AWD_ENEMYDRIVEBYKILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_USJS_COMPLETED"), 50, 0);
					STATS::STAT_SET_INT($("MP0_USJS_FOUND"), 50, 0);
					STATS::STAT_SET_BOOL($("MP0_AWD_FMWINALLRACEMODES"), 1, 0);
					STATS::STAT_SET_BOOL($("MP0_AWD_FMWINEVERYGAMEMODE"), 1, 0);
					STATS::STAT_SET_INT($("MP0_DB_PLAYER_KILLS"), 1000, 0);
					STATS::STAT_SET_INT($("MP0_KILLS_PLAYERS"), 1000, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FMHORDWAVESSURVIVE"), 21, 0);
					STATS::STAT_SET_INT($("MP0_AWD_CAR_BOMBS_ENEMY_KILLS"), 25, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_TDM_MVP"), 60, 0);
					STATS::STAT_SET_INT($("MP0_AWD_HOLD_UP_SHOPS"), 20, 0);
					STATS::STAT_SET_INT($("MP0_AWD_RACES_WON"), 101, 0);
					STATS::STAT_SET_INT($("MP0_AWD_NO_ARMWRESTLING_WINS"), 21, 0);
					STATS::STAT_SET_BOOL($("MP0_AWD_FMATTGANGHQ"), 1, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FMBBETWIN"), 50000, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_DM_WINS"), 51, 0);
					STATS::STAT_SET_BOOL($("MP0_AWD_FMFULLYMODDEDCAR"), 1, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_DM_TOTALKILLS"), 500, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_DM_TOTAL_DEATHS"), 412, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TIMES_FINISH_DM_TOP_3"), 36, 0);
					STATS::STAT_SET_INT($("MP0_PLAYER_HEADSHOTS"), 623, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_DM_WINS"), 63, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_TDM_WINS"), 13, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_GTA_RACES_WON"), 12, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_GOLF_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_TG_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_RT_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_CT_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_GRAN_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_AWD_FM_TENNIS_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TENNIS_MATCHES_WON"), 2, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TOTAL_TDEATHMATCH_WON"), 63, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TOTAL_RACES_WON"), 101, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TOTAL_DEATHMATCH_LOST"), 23, 0);
					STATS::STAT_SET_INT($("MP0_MPPLY_TOTAL_RACES_LOST"), 36, 0);
					STATS::STAT_SET_INT($("MP0_AWD_25_KILLS_STICKYBOMBS"), 50, 0);
					STATS::STAT_SET_INT($("MP0_AWD_50_KILLS_GRENADES"), 50, 0);
					STATS::STAT_SET_INT($("MP0_GRENADE_ENEMY_KILLS "), 50, 0);
					STATS::STAT_SET_INT($("MP0_AWD_20_KILLS_MELEE"), 50, 0);
				}
				break;
			case 4:
				//tattoooss
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_1"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_2"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_3"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_4"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_5"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_6"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_7"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_8"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_9"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_10"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_11"), -1, 1);
					STATS::STAT_SET_INT($("MP0_tattoo_fm_unlocks_12"), -1, 1);
				}
				break;
			case 5:
				//snacks/armor
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_NO_BOUGHT_YUM_SNACKS"), 30, 0);
					STATS::STAT_SET_INT($("MP0_NO_BOUGHT_HEALTH_SNACKS"), 30, 0);
					STATS::STAT_SET_INT($("MP0_NO_BOUGHT_EPIC_SNACKS"), 30, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_ARMOUR_1_COUNT"), 10, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_ARMOUR_2_COUNT"), 10, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_ARMOUR_3_COUNT"), 10, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_ARMOUR_4_COUNT"), 10, 0);
					STATS::STAT_SET_INT($("MP_CHAR_ARMOUR_5_COUNT"), 10, 0);
				}
				break;
			case 6:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E2"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E3"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E4"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E5"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E6"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E7"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E8"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E9"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E10"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E11"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_FM_PURCHAS E12"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_1_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_2_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_3_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_4_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_5_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_6_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_7_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_8_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_9_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_10_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_11_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_KIT_12_FM_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_races_won"), 100, 0);
					STATS::STAT_SET_INT($("MP0_number_turbo_starts_in_race"), 100, 0);
					STATS::STAT_SET_INT($("MP0_usjs_found"), 50, 0);
					STATS::STAT_SET_INT($("MP0_usjs_completed"), 50, 0);
					STATS::STAT_SET_INT($("MP0_awd_fmwinairrace"), 50, 0);
					STATS::STAT_SET_INT($("MP0_awd_fmwinsearace"), 50, 0);
					STATS::STAT_SET_INT($("MP0_awd_fmrallywonnav"), 50, 0);
					STATS::STAT_SET_INT($("MP0_awd_fmrallywondrive "), 500, 0);
					STATS::STAT_SET_INT($("MP0_awd_fm_races_fastest_lap"), 500, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_0_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_1_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_2_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_3_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_4_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_5_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_6_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_char_fm_carmod_7_unlck"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_VEHICLE_1_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_VEHICLE_2_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_ABILITY_1_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_ABILITY_2_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_ABILITY_3_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_1_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_2_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_3_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_4_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_5_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_6_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_7_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_8_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_PACKAGE_9_COLLECT"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_HEALTH_1_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_CHAR_FM_HEALTH_2_UNLCK"), -1, 0);
					STATS::STAT_SET_INT($("MP0_HOLDUPS_BITSET"), -1, 0);
				}
				break;
			case 7:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_MOLOTOV_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_CMBTPISTOL_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_PISTOL50_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_APPISTOL_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_MICROSMG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_SMG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_ASLTSMG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_ASLTRIFLE_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_CRBNRIFLE_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_ADVRIFLE_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_MG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_CMBTMG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_ASLTMG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_PUMP_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_SAWNOFF_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_BULLPUP_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_ASLTSHTGN_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_SNIPERRFL_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_HVYSNIPER_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_GRNLAUNCH_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_RPG_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_MINIGUNS_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_GRENADE_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_SMKGRENADE_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_STKYBMB_ENEMY_KILLS"), 600, 0);
					STATS::STAT_SET_INT($("MP0_MOLOTOV_ENEMY_KILLS"), 600, 0);
				}
				break;
			case 8:
				STATS::STAT_SET_INT($("MP0_TOTAL_PLAYING_TIME"), 792000000, true);
				STATS::STAT_SET_INT($("MP1_TOTAL_PLAYING_TIME"), 792000000, true);
				STATS::STAT_SET_INT($("MP2_TOTAL_PLAYING_TIME"), 792000000, true);
				break;
			case 9:
				STATS::STAT_SET_INT($("MP0_AWD_WIN_CAPTURES"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_DROPOFF_CAP_PACKAGES"), 100, 1);
				STATS::STAT_SET_INT($("MP0_AWD_KILL_CARRIER_CAPTURE"), 100, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FINISH_HEISTS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FINISH_HEIST_SETUP_JOB"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_NIGHTVISION_KILLS, 100"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_WIN_LAST_TEAM_STANDINGS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_ONLY_PLAYER_ALIVE_LTS"), 50, 1);
				break;
			case 10:
				STATS::STAT_SET_INT($("MP0_AWD_FMHORDWAVESSURVIVE"), 10, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMPICKUPDLCCRATE1ST"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_WIN_CAPTURE_DONT_DYING"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_DO_HEIST_AS_MEMBER"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_PICKUP_CAP_PACKAGES"), 100, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FINISH_HEIST_NO_DAMAGE"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_WIN_GOLD_MEDAL_HEISTS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_KILL_TEAM_YOURSELF_LTS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_DO_HEIST_AS_THE_LEADER"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_STORE_20_CAR_IN_GARAGES"), 1, 1);
				break;
			case 11:
				STATS::STAT_SET_BOOL($("MP0_FM_CHANGECHAR_ASKED"), 0, 1);
				break;
			case 12:
				STATS::STAT_SET_BOOL($("MP1_FM_CHANGECHAR_ASKED"), 0, 1);
				break;
			case 13:
				STATS::STAT_SET_BOOL($("MP0_NO_MORE_TUTORIALS"), 1, 1);
				break;
			case 14:
				STATS::STAT_SET_INT($("MP0_PLAYER_HEADSHOTS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_PISTOL_ENEMY_KILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_SAWNOFF_ENEMY_KILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_MICROSMG_ENEMY_KILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_SNIPERRFL_ENEMY_KILLS"), 100, 1);
				STATS::STAT_SET_INT($("MP0_UNARMED_ENEMY_KILLS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_STKYBMB_ENEMY_KILLS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_GRENADE_ENEMY_KILLS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_RPG_ENEMY_KILLS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_CARS_EXPLODED"), 500, 1);
				STATS::STAT_SET_INT($("MP0_AWD_5STAR_WANTED_AVOIDANCE"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_CAR_BOMBS_ENEMY_KILLS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_CARS_EXPORTED"), 50, 1);
				STATS::STAT_SET_INT($("MP0_PASS_DB_PLAYER_KILLS"), 100, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_DM_WINS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_GOLF_WON"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_GTA_RACES_WON"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_CT_WON"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_RT_WON"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_SHOOTRANG_TG_WON"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_TDM_WINS"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_TENNIS_WON"), 25, 1);
				STATS::STAT_SET_INT($("MP0_MOST_SPINS_IN_ONE_JUMP"), 5, 1);
				STATS::STAT_SET_INT($("MPPLY_AWD_FM_CR_DM_MADE"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMHORDWAVESSURVIVE"), 10, 1);
				STATS::STAT_SET_INT($("MP0_AWD_HOLD_UP_SHOPS"), 20, 1);
				STATS::STAT_SET_INT($("MP0_ASLTRIFLE_ENEMY_KILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_MG_ENEMY_KILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_AWD_LAPDANCES"), 25, 1);
				STATS::STAT_SET_INT($("MP0_MOST_ARM_WRESTLING_WINS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_NO_HAIRCUTS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_RACES_WON"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_SECURITY_CARS_ROBBED"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_VEHICLES_JACKEDR"), 500, 1);
				STATS::STAT_SET_INT($("MP0_MOST_FLIPS_IN_ONE_JUMP"), 5, 1);
				STATS::STAT_SET_INT($("MP0_AWD_WIN_AT_DARTS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_PASSENGERTIME"), 4, 1);
				STATS::STAT_SET_INT($("MP0_AWD_TIME_IN_HELICOPTER"), 4, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_DM_3KILLSAMEGUY"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_DM_KILLSTREAK"), 100, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_DM_STOLENKILL"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_DM_TOTALKILLS"), 500, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_GOLF_BIRDIES"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_GOLF_HOLE_IN_1"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_RACE_LAST_FIRST, 25"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_RACES_FASTEST_LAP, 25"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM_SHOOTRANG_GRAN_WON"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_TDM_MVP"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FM_TENNIS_ACE"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM_TENNIS_STASETWIN"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM6DARTCHKOUT"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMATTGANGHQ"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_PARACHUTE_JUMPS_20M"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_PARACHUTE_JUMPS_50M"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AIR_LAUNCHES_OVER_40M"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_BUY_EVERY_GUN"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMWINEVERYGAMEMODE"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMDRIVEWITHOUTCRASH"), 255, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMCRATEDROPS"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM25DIFFERENTDM"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM_TENNIS_5_SET_WINS"), 1, 1);
				STATS::STAT_SET_INT($("MPPLY_AWD_FM_CR_PLAYED_BY_PEEP"), 100, 1);
				STATS::STAT_SET_INT($("MPPLY_AWD_FM_CR_RACES_MADE"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM25DIFFERENTRACES"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FM25DIFITEMSCLOTHES"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMFULLYMODDEDCAR"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMKILLBOUNTY"), 25, 1);
				STATS::STAT_SET_INT($("MP0_KILLS_PLAYERS"), 1000, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMPICKUPDLCCRATE1ST"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMSHOOTDOWNCOPHELI"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMKILL3ANDWINGTARACE"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMKILLSTREAKSDM"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMMOSTKILLSGANGHIDE"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMMOSTKILLSSURVIVE"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMRACEWORLDRECHOLDER"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMRALLYWONDRIVE"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMRALLYWONNAV"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMREVENGEKILLSDM"), 50, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMWINAIRRACE"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMWINCUSTOMRACE"), 1, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMWINRACETOPOINTS"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMWINSEARACE"), 25, 1);
				STATS::STAT_SET_INT($("MP0_AWD_FMBASEJMP"), 25, 1);
				STATS::STAT_SET_BOOL($("MP0_MP0_AWD_FMWINALLRACEMODES"), 1, 1);
				STATS::STAT_SET_BOOL($("MP0_AWD_FMTATTOOALLBODYPARTS"), 1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_WANTED_LEVEL_TIME5STAR"), 2147483647, 1);
				STATS::STAT_SET_FLOAT($("MP0_LONGEST_WHEELIE_DIST"), 1000, 1);
				break;
			case 15:
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_1"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_3"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_4"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_5"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_6"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CLTHS_AVAILABLE_HAIR_7"), -1, 1);
				break;
			case 16:
				STATS::STAT_SET_INT($("MP0_ADMIN_WEAPON_GV_BS_1"), -1, 1);
				STATS::STAT_SET_INT($("MP0_ADMIN_WEAPON_GV_BS_2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_ADMIN_WEAPON_GV_BS_3"), -1, 1);
				STATS::STAT_SET_INT($("MP0_BOTTLE_IN_POSSESSION"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_UNLOCKED"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_UNLOCKED2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_WEAP_FM_PURCHASE"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_WEAP_FM_PURCHASE2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_ADDON_1_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_ADDON_2_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_ADDON_3_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_ADDON_4_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_CHAR_FM_WEAP_ADDON_5_UNLCK"), -1, 1);
				STATS::STAT_SET_INT($("MP0_WEAP_FM_ADDON_PURCH"), -1, 1);
				STATS::STAT_SET_INT($("MP0_WEAP_FM_ADDON_PURCH2"), -1, 1);
				STATS::STAT_SET_INT($("MP0_WEAP_FM_ADDON_PURCH3"), -1, 1);
				STATS::STAT_SET_INT($("MP0_WEAP_FM_ADDON_PURCH4"), -1, 1);
				STATS::STAT_SET_INT($("MP0_WEAP_FM_ADDON_PURCH5"), -1, 1);
				break;
			case 17:
				STATS::STAT_SET_INT($("MPPLY_VEHICLE_ID_ADMIN_WEB"), 117401876, 1);
				break;
			case 18:
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_1ST_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_2ND_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_3RD_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_4TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_5TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_6TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_7TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_8TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_9TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("BADSPORT_NUMDAYS_10TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_1ST_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_2ND_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_3RD_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_4TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_5TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_6TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_7TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_8TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_9TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP0_BADSPORT_NUMDAYS_10TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_1ST_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_2ND_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_3RD_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_4TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_5TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_6TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_7TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_8TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_9TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP1_BADSPORT_NUMDAYS_10TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_1ST_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_2ND_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_3RD_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_4TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_5TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_6TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_7TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_8TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_9TH_OFFENCE"), 0, true);
				STATS::STAT_SET_INT($("MP2_BADSPORT_NUMDAYS_10TH_OFFENCE"), 0, true);
				STATS::STAT_SET_FLOAT($("BADSPORT_RESET_MINUTES"), 1.0, true);
				STATS::STAT_SET_FLOAT($("MP0_BADSPORT_RESET_MINUTES"), 1.0, true);
				STATS::STAT_SET_FLOAT($("MP1_BADSPORT_RESET_MINUTES"), 1.0, true);
				STATS::STAT_SET_FLOAT($("MP2_BADSPORT_RESET_MINUTES"), 1.0, true);
				STATS::STAT_SET_FLOAT($("MP0_MPPLY_OVERALL_BADSPORT"), 0, true);
				STATS::STAT_SET_BOOL($("MP0_MPPLY_CHAR_IS_BADSPORT"), false, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_BECAME_BADSPORT_NUM"), 0, true);
				STATS::STAT_SET_INT($("MP0_BAD_SPORT_BITSET"), 0, true);
				break;
			case 19:
				STATS::STAT_SET_INT($("MP0_CHEAT_BITSET"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_REPORT_STRENGTH"), 32, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_COMMEND_STRENGTH"), 100, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_FRIENDLY"), 100, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_HELPFUL"), 100, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_GRIEFING"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_OFFENSIVE_LANGUAGE"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_OFFENSIVE_UGC"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_VC_HATE"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_GAME_EXPLOITS"), 0, true);
				STATS::STAT_SET_INT($("MP0_MPPLY_ISPUNISHED"), 0, true);
				STATS::STAT_SET_FLOAT($("MP1_MPPLY_OVERALL_BADSPORT"), 0, true);
				STATS::STAT_SET_BOOL($("MP1_MPPLY_CHAR_IS_BADSPORT"), false, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_BECAME_BADSPORT_NUM"), 0, true);
				STATS::STAT_SET_INT($("MP1_BAD_SPORT_BITSET"), 0, true);
				STATS::STAT_SET_INT($("MP1_CHEAT_BITSET"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_REPORT_STRENGTH"), 32, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_COMMEND_STRENGTH"), 100, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_FRIENDLY"), 100, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_HELPFUL"), 100, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_GRIEFING"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_OFFENSIVE_LANGUAGE"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_OFFENSIVE_UGC"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_VC_HATE"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_GAME_EXPLOITS"), 0, true);
				STATS::STAT_SET_INT($("MP1_MPPLY_ISPUNISHED"), 0, true);
				STATS::STAT_SET_FLOAT($("MP2_MPPLY_OVERALL_BADSPORT"), 0, true);
				STATS::STAT_SET_BOOL($("MP2_MPPLY_CHAR_IS_BADSPORT"), false, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_BECAME_BADSPORT_NUM"), 0, true);
				STATS::STAT_SET_INT($("MP2_BAD_SPORT_BITSET"), 0, true);
				STATS::STAT_SET_INT($("MP2_CHEAT_BITSET"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_REPORT_STRENGTH"), 32, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_COMMEND_STRENGTH"), 100, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_FRIENDLY"), 100, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_HELPFUL"), 100, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_GRIEFING"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_OFFENSIVE_LANGUAGE"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_OFFENSIVE_UGC"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_VC_HATE"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_GAME_EXPLOITS"), 0, true);
				STATS::STAT_SET_INT($("MP2_MPPLY_ISPUNISHED"), 0, true);
				break;
				// switchable features
			default:
				if (lines[activeLineIndexRec].pState)
					*lines[activeLineIndexRec].pState = !(*lines[activeLineIndexRec].pState);
				if (lines[activeLineIndexRec].pUpdated)
					*lines[activeLineIndexRec].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexRec == 0)
						activeLineIndexRec = lineCount;
					activeLineIndexRec--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexRec++;
						if (activeLineIndexRec == lineCount)
							activeLineIndexRec = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexLevel = 0;

void process_level_menu()
{
	const float lineWidth = 250.0;
	const int lineCount = 16;

	std::string caption = "Level  Options";

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Level 50 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 100 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 120 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 150 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 200 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 250 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 300 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 350 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 400 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 450 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 500 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 600 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 700 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 800 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 900 ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Level 999 ~HUD_COLOUR_GREEN~:", NULL, NULL }
	};

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexLevel)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexLevel].text, lines[activeLineIndexLevel].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexLevel * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			Ped playerGroup = PED::GET_PED_GROUP_INDEX(PLAYER::PLAYER_PED_ID());
			menu_beep(NAV_SELECT);
			switch (activeLineIndexLevel)
			{
			case 0:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 448800, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 448800, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 448800, true);
				}
				break;
			case 1:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 1584350, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 1584350, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 1584350, true);
				}
				break;
			case 2:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 2165850, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 2165850, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 2165850, true);
				}
				break;
			case 3:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 3075600, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 3075600, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 3075600, true);
				}
				break;
			case 4:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 4691850, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 4691850, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 4691850, true);
				}
				break;
			case 5:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 6433100, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 6433100, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 6433100, true);
				}
				break;
			case 6:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 8299350, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 8299350, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 8299350, true);
				}
				break;
			case 7:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 10290600, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 10290600, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 10290600, true);
				}
				break;
			case 8:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 12406850, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 12406850, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 12406850, true);
				}
				break;
			case 9:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 14648100, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 14648100, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 14648100, true);
				}
				break;
			case 10:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 17014350, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 17014350, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 17014350, true);
				}
				break;
			case 11:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 22121850, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 22121850, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 22121850, true);
				}
				break;
			case 12:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 27729350, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 27729350, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 27729350, true);
				}
				break;
			case 13:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 33836850, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 33836850, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 33836850, true);
				}
				break;
			case 14:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 40444350, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 40444350, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 40444350, true);
				}
				break;
			case 15:
				if (ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID()))
				{
					STATS::STAT_SET_INT($("MP0_CHAR_XP_FM"), 47478300, true);
					STATS::STAT_SET_INT($("MP1_CHAR_XP_FM"), 47478300, true);
					STATS::STAT_SET_INT($("MP2_CHAR_XP_FM"), 47478300, true);
				}
				break;
				// switchable features
			default:
				if (lines[activeLineIndexLevel].pState)
					*lines[activeLineIndexLevel].pState = !(*lines[activeLineIndexLevel].pState);
				if (lines[activeLineIndexLevel].pUpdated)
					*lines[activeLineIndexLevel].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexLevel == 0)
						activeLineIndexLevel = lineCount;
					activeLineIndexLevel--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexLevel++;
						if (activeLineIndexLevel == lineCount)
							activeLineIndexLevel = 0;
						waitTime = 150;
					}

	}
}


int activeLineIndexoplayers = 0;

void process_oplayers1(int index2)
{
	const float lineWidth = 250.0;
	const int lineCount = 2;

	std::string caption = PLAYER::GET_PLAYER_NAME(PLAYER::INT_TO_PLAYERINDEX(index2));

	static struct {
		LPCSTR		text;
		bool		*pState;
		bool		*pUpdated;
	} lines[lineCount] = {
		{ "Recieve All Weapons ~HUD_COLOUR_GREEN~:", NULL, NULL },
		{ "Remove All Weapons ~HUD_COLOUR_GREEN~:", NULL, NULL },

	};


	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i < lineCount; i++)
				if (i != activeLineIndexoplayers)
					draw_menu_line(line_as_str(lines[i].text, lines[i].pState),
						lineWidth, 9.0f, 60.0f + i * 30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(line_as_str(lines[activeLineIndexoplayers].text, lines[activeLineIndexoplayers].pState),
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexoplayers * 30.0f, 1000.0f, 7.0f, true, false);

			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;
		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			Ped playerGroup = PED::GET_PED_GROUP_INDEX(PLAYER::PLAYER_PED_ID());
			BOOL bPlayerExists = ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_PED_ID());
			Player player = PLAYER::PLAYER_ID();
			Ped playerPed = PLAYER::PLAYER_PED_ID();

			Player selectedPlayer = PLAYER::GET_PLAYER_PED(index2);
			BOOL bSelectedPlayerExists = ENTITY::DOES_ENTITY_EXIST(selectedPlayer);

			Vector3 playerPedPos = ENTITY::GET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), 0);
			Vector3 selectedPlayerPos = ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED(0), 0);

			Vehicle playerPedVeh = PED::GET_VEHICLE_PED_IS_USING(playerPed);
			Vehicle selectedPlayerVeh = PED::GET_VEHICLE_PED_IS_USING(selectedPlayer);
			menu_beep(NAV_SELECT);
			switch (activeLineIndexoplayers)
			{
			case 0:
				for (int i = 0; i < sizeof(weaponNames) / sizeof(weaponNames[0]); i++)
					WEAPON::GIVE_DELAYED_WEAPON_TO_PED(selectedPlayer, $((char *)weaponNames[i]), 1000, 1);
				ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED(index2), 0);
				break;
			case 1:
				WEAPON::REMOVE_ALL_PED_WEAPONS(selectedPlayer, 1);
				ENTITY::GET_ENTITY_COORDS(PLAYER::GET_PLAYER_PED(index2), 0);
				break;
				// switchable features
			default:
				if (lines[activeLineIndexoplayers].pState)
					*lines[activeLineIndexoplayers].pState = !(*lines[activeLineIndexoplayers].pState);
				if (lines[activeLineIndexoplayers].pUpdated)
					*lines[activeLineIndexoplayers].pUpdated = true;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexoplayers == 0)
						activeLineIndexoplayers = lineCount;
					activeLineIndexoplayers--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexoplayers++;
						if (activeLineIndexoplayers == lineCount)
							activeLineIndexoplayers = 0;
						waitTime = 150;
					}

	}
}


int activeLineIndexoplayers2 = 0;

void process_oplayers2()
{
	const float lineWidth = 250.0;
	const int lineCount = 32;

	std::string caption = "Player  Selection";

	typedef struct {
		std::string		name;
		Ped				ped;
		int			playerInt;
	} lines;

	lines onlinePlayers[32];

	for (int i = 0; i < lineCount; i++)
		onlinePlayers[i] = { PLAYER::GET_PLAYER_NAME(PLAYER::INT_TO_PLAYERINDEX(i)), PLAYER::GET_PLAYER_PED(i), i };

	DWORD waitTime = 150;
	while (true)
	{
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do
		{
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0, 20.0, 1000.0, 5.0, false, true);
			for (int i = 0; i < lineCount; i++)

				if (i != activeLineIndexoplayers2)
					draw_menu_line(onlinePlayers[i].name,
						lineWidth, 2.0f, 60.0f + i * 20.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(onlinePlayers[activeLineIndexoplayers2].name,
				lineWidth, 2.0f, 60.0f + activeLineIndexoplayers2 * 20.0f, 1000.0f, 7.0f, true, false);


			update_features();
			WAIT(0);
		} while (GetTickCount() < maxTickCount);
		waitTime = 0;

		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect)
		{
			menu_beep(NAV_SELECT);
			if (onlinePlayers[activeLineIndexoplayers2].name.compare("~HUD_COLOUR_RED~**INVALID**") != 0)
				process_oplayers1(onlinePlayers[activeLineIndexoplayers2].playerInt);
			else
			{
				set_status_text("~HUD_COLOUR_RED~Invalid Player");
				NotifyBottomCentre("~HUD_COLOUR_RED~Invalid Player");

			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed())
			{
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp)
				{
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexoplayers2 == 0)
						activeLineIndexoplayers2 = lineCount;
					activeLineIndexoplayers2--;
					waitTime = 150;
				}
				else
					if (bDown)
					{
						menu_beep(NAV_UP_DOWN);
						activeLineIndexoplayers2++;
						if (activeLineIndexoplayers2 == lineCount)
							activeLineIndexoplayers2 = 0;
						waitTime = 150;
					}
	}
}

int activeLineIndexMain = 0;

void process_main_menu() {
	const float lineWidth = 250.0;
	const int lineCount = 11;

	std::string caption = "SudoMod ~HUD_COLOUR_BLUE~Infamous ~HUD_COLOUR_RED~Dev Team";

	static LPCSTR lineCaption[lineCount] = {
		"Player ~HUD_COLOUR_BLUE~>",
		"Teleportation ~HUD_COLOUR_BLUE~>",
		"Weapon ~HUD_COLOUR_BLUE~>",
		"Vehicle ~HUD_COLOUR_BLUE~>",
		"World ~HUD_COLOUR_BLUE~>",
		"Time ~HUD_COLOUR_BLUE~>",
		"Weather ~HUD_COLOUR_BLUE~>",
		"Miscellaneous ~HUD_COLOUR_BLUE~>",
		"Level ~HUD_COLOUR_BLUE~>",
		"Recovery ~HUD_COLOUR_BLUE~>",
		"Online ~HUD_COLOUR_BLUE~>" };
	DWORD waitTime = 150;
	while (true) {
		// timed menu draw, used for pause after active line switch
		DWORD maxTickCount = GetTickCount() + waitTime;
		do {
			// draw menu
			draw_menu_line(caption, lineWidth, 15.0f, 20.0f, 1000.0f, 5.0f, false, true);
			for (int i = 0; i<lineCount; i++)
				if (i != activeLineIndexMain)
					draw_menu_line(lineCaption[i],
						lineWidth, 9.0f, 60.0f + i*30.0f, 1000.0f, 7.0f, false, false);
			draw_menu_line(lineCaption[activeLineIndexMain],
				lineWidth + 0.0f, 9.0f, 60.0f + activeLineIndexMain*30.0f, 1000.0f, 7.0f, true, false);
			update_features();
			WAIT(0);
		} while (GetTickCount()<maxTickCount);
		waitTime = 0;
		// process buttons
		bool bSelect, bBack, bUp, bDown;
		get_button_state(&bSelect, &bBack, &bUp, &bDown, NULL, NULL);
		if (bSelect) {
			menu_beep(NAV_SELECT);
			switch (activeLineIndexMain) {
			case 0:
				process_player_menu();
				break;
			case 1:
				notifyAboveMap("~HUD_COLOUR_YELLOW~Teleport: ~HUD_COLOUR_WHITE~Num 1			Close Menu, and set a Waypoint!");
				NotifyBottomCentre("~HUD_COLOUR_YELLOW~Teleport: ~HUD_COLOUR_WHITE~Num 1 Close Menu, and set a Waypoint!");
				process_teleport_menu();
				break;
			case 2:
				notifyAboveMap("~HUD_COLOUR_ORANGE~Vehicle Rockets: ~HUD_COLOUR_WHITE~W Key");
				notifyAboveMap("~HUD_COLOUR_ORANGE~Throw Knife RPG: ~HUD_COLOUR_WHITE~E Key");
				process_weapon_menu();
				break;
			case 3:
				process_veh_menu();
				break;
			case 4:
				process_world_menu();
				break;
			case 5:
				process_time_menu();
				break;
			case 6:
				process_weather_menu();
				break;
			case 7:
				notifyAboveMap("~HUD_COLOUR_WHITE~Miscellaneous Options");
				process_misc_menu();
				break;
			case 8:
				notifyAboveMap("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~High-Risk Features!");
				NotifyBottomCentre("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~Level Options Are High-Risk Features!");
				process_level_menu();
				break;
			case 9:
				notifyAboveMap("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~High-Risk Features!");
				NotifyBottomCentre("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~Recovery Options Are High-Risk Features!");
				process_rec_menu();
				break;
			case 10:
				notifyAboveMap("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~High-Risk Features!");
				NotifyBottomCentre("~HUD_COLOUR_RED~*Warning* ~HUD_COLOUR_WHITE~Online Options Are High-Risk Features!");
				process_oplayers2();
				break;
			}
			waitTime = 200;
		}
		else
			if (bBack || trainer_switch_pressed()) {
				menu_beep(NAV_CANCEL);
				break;
			}
			else
				if (bUp) {
					menu_beep(NAV_UP_DOWN);
					if (activeLineIndexMain == 0)
						activeLineIndexMain = lineCount;
					activeLineIndexMain--;
					waitTime = 150;
				}
				else
					if (bDown) {
						menu_beep(NAV_UP_DOWN);
						activeLineIndexMain++;
						if (activeLineIndexMain == lineCount)
							activeLineIndexMain = 0;
						waitTime = 150;
					}
	}
}

int CH_Teleport_Active = 0;
bool Teleport() {
	Entity e = PLAYER::PLAYER_PED_ID();
	if (PED::IS_PED_IN_ANY_VEHICLE(e, 0))
		e = PED::GET_VEHICLE_PED_IS_USING(e);
	// Get Coordinates
	Vector3 coords;
	bool CH_Teleport_Success = false;
	// Search For Marker Blip
	if (CH_Teleport_Active == 0) {
		bool CH_Blip_Found = false;
		int CH_Blip_Iterator = UI::_GET_BLIP_INFO_ID_ITERATOR();
		for (Blip i = UI::GET_FIRST_BLIP_INFO_ID(CH_Blip_Iterator);
			UI::DOES_BLIP_EXIST(i) != 0; i = UI::GET_NEXT_BLIP_INFO_ID(CH_Blip_Iterator)) {
			if (UI::GET_BLIP_INFO_ID_TYPE(i) == 4) {
				coords = UI::GET_BLIP_INFO_ID_COORD(i);
				CH_Blip_Found = true;
				break;
			}
		}
		// Load Map Region & Check Height Levels For Existence Ground
		if (CH_Blip_Found) {
			bool CH_Ground_Found = false;
			static float groundCheckHeight[] = {
				100.0,150.0,50.0,0.0,200.0,250.0,300.0,350.0,400.0,
				450.0,500.0,550.0,600.0,650.0,700.0,750.0,800.0 };
			for (int i = 0; i<sizeof(groundCheckHeight) / sizeof(float); i++) {
				ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, groundCheckHeight[i], 0, 0, 1);
				WAIT(100);
				if (GAMEPLAY::GET_GROUND_Z_FOR_3D_COORD(coords.x, coords.y, groundCheckHeight[i], &coords.z, 0)) {
					CH_Ground_Found = true;
					coords.z += 3.0;
					break;
				}
			}
			if (!CH_Ground_Found) {
				coords.z = 1000.0;
			}
			CH_Teleport_Success = true;
		}
		if (CH_Teleport_Success) {
			ENTITY::SET_ENTITY_COORDS_NO_OFFSET(e, coords.x, coords.y, coords.z, 0, 0, 1);
			WAIT(0);
			set_status_text("~HUD_COLOUR_GREEN~Teleport complete");
			return true;
		}
	}
	return false;
}

void kteleport() {
	if (KeyJustUp((VK_NUMPAD1))) {
		Teleport();
	}
	return;
	featureTeleportKey = true;
}

void reset_globals() {
	activeLineIndexMain =
		activeLineIndexPlayer =
		activeLineIndexoplayers =
		activeLineIndexoplayers2 =
		skinchangerActiveLineIndex =
		skinchangerActiveItemIndex =
		teleportActiveLineIndex =
		activeLineIndexWeapon =
		activeLineIndexOWeapon =
		activeLineIndexVeh =
		activeLineIndexCarMenu =
		activeLineIndexPaint =
		carspawnActiveLineIndex =
		carspawnActiveItemIndex =
		activeLineIndexHUD =
		activeLineIndexWorld =
		activeLineIndexWeather = 0;

	featurePlayerInvincible =
		featurePlayerInvincibleUpdated =
		featurePlayerNeverWanted =
		featurePlayerIgnored =
		featurePlayerIgnoredUpdated =
		featurePlayerUnlimitedAbility =
		featurePlayerNoNoise =
		featurePlayerNoNoiseUpdated =
		featurePlayerFastSwim =
		featurePlayerFastSwimUpdated =
		featurePlayerFastRun =
		featurePlayerFastRunUpdated =
		featurePlayerSuperJump =

		featureWeaponNoReload =
		featureWeaponFireAmmo =
		featureWeaponExplosiveAmmo =
		featureWeaponExplosiveMelee =
		featureWeaponVehRockets =
		featureObjectGun =
		featureObjectGun1 =
		featureObjectGun2 =

		featureVehInvincible =
		featureVehInvincibleUpdated =
		featureVehInvincibleWheels =
		featureVehInvincibleWheelsUpdated =
		featurePlayerInvisible =
		featureVehSeatbelt =
		featureVehSeatbeltUpdated =
		featureVehSpeedBoost =
		featureVehWrapInSpawned =

		featureWorldMoonGravity =
		featureTimePaused =
		featureTimePausedUpdated =
		featureTimeSynced =

		featureWeatherWind =
		featureWeatherPers =
		featureMiscLockRadio =
		featureMiscHideHud = false;
	pedspawnActiveLineIndex =
		pedspawnActiveItemIndex =

		featureWorldRandomCops =
		featureWorldRandomTrains =
		featureWorldRandomBoats =
		featureWorldGarbageTrucks = true;
	featureWorldBlackOut =

		skinchanger_used = false;
}

void main() {

	reset_globals();
	while (true) {
		if (trainer_switch_pressed()) {
			static bool first_time = false;
			if (!first_time)
			{
				NotifyBottomCentre("SudoMod ~HUD_COLOUR_BLUE~Infamous ~HUD_COLOUR_RED~Dev Team");
				notifyAboveMap("~HUD_COLOUR_BLUE~www.infamouscheats.cc");
				first_time = true;
			}
			
			menu_beep(NAV_OPEN);
			process_main_menu();
		}
		update_features();
		kteleport();
		WAIT(0);
	}
}

void ScriptMain() {
	srand(GetTickCount());
	main();
}















